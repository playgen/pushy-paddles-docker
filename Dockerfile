FROM ubuntu:16.04

RUN apt-get update && apt-get -y install nginx netcat unzip

COPY . /usr/src/pushypaddles
WORKDIR /usr/src/pushypaddles

COPY ./default /etc/nginx/sites-available/default

WORKDIR Unity/Builds
RUN unzip Game.zip
RUN unzip Server.zip

RUN chmod +x Server.x86_64

WORKDIR ../../src
RUN unzip PlayGen.Orchestrator.PSL.Service

WORKDIR ../PlayGen.Orchestrator.PSL.Service
RUN chmod +x startup.sh

ENTRYPOINT ./startup.sh
