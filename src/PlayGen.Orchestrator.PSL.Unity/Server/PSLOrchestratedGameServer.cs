﻿using System;
using System.Collections.Generic;
using System.Linq;

using PlayGen.Orchestratror.Unity.Server;
using PlayGen.Orchestrator.PSL.Common.LRS;
using PlayGen.Orchestrator.PSL.Contracts.LRS;
using PlayGen.Unity.AsyncUtilities;

namespace PlayGen.Orchestrator.PSL.Unity.Server
{
	public class PSLOrchestratedGameServer : OrchestratedGameServer
	{
		// This class will contain a list of players and their current skills, ready for sending at the end of the game. Skills will be incremented and normalised as appropriate
		// Eg. if a player is cooperative, cooperative +1 then the most cooperative score at the end will be closest to 1

		private Dictionary<LRSPlayerSkill, int> _playerSkills = new Dictionary<LRSPlayerSkill, int>();

		private Dictionary<LRSPlayerSkill, int> _verbCount = new Dictionary<LRSPlayerSkill, int>();

		public void AddSkill(string playerId, LRSSkillVerb skill, int increment)
		{
			var playerSkill = new LRSPlayerSkill
			{
				Id = playerId,
				Skill = skill
			};

			var key = _playerSkills.Keys.FirstOrDefault(p => p.Equals(playerSkill));

			if (key != null)
			{
				_playerSkills[key] += increment;
				if (_playerSkills[key] <= 0)
				{
					_playerSkills[key] = 0;
				}
			}
			else
			{
				_playerSkills.Add(playerSkill, increment);
			}

			key = _verbCount.Keys.FirstOrDefault(p => p.Equals(playerSkill));

			if (key != null)
			{
				// incrememnt the total possible
				_verbCount[key] += 1;
			}
			else
			{
				_verbCount.Add(playerSkill, 1);
			}
		}

		public void SendStoredLRSData()
		{
			foreach (var playerSkill in _playerSkills)
			{
				var id = playerSkill.Key.Id;
				var skill = playerSkill.Key.Skill;
				var value = GetNormalizedValue(playerSkill.Key);
				SendLRSData(id, skill, value);
			}
		}

		public void SendLRSData(string playerId, LRSSkillVerb verb, float value)
		{
			LogProxy.Info($"{nameof(OrchestratedGameServer)}::{nameof(SendLRSData)} [instanceId {Config.GameInstanceId}, player: {playerId}, verb: {verb}, value: {value}]");
			var path = "game/lrs";
			var dataRequest = new LRSDataRequest
								{
									MatchId = Config.GameInstanceId.ToString(),
									PlayerId = playerId,
									Verb = verb,
									Value = value
								};
			ClientImplementation.ServicePOST(path, dataRequest, () => LogProxy.Info("LRS Data Sent"), () => throw new InvalidOperationException("Error uploading LRS data with orchestrator"));
		}

		public string OutputSkillData()
		{
			var data = "";

			foreach (var playerSkill in _playerSkills)
			{
				var id = playerSkill.Key.Id;
				var skill = playerSkill.Key.Skill;

				var value = GetNormalizedValue(playerSkill.Key);

				data += string.Format("\nPlayer: {0}, showed skill: {1}, with value: {2}", id, skill, value);
			}
			return data;
		}

		private float GetNormalizedValue(LRSPlayerSkill playerSkill)
		{
			var totalValue = _verbCount[playerSkill];

			var normalized = GetNormalized(_playerSkills[playerSkill], totalValue, 0, playerSkill.Skill.GetMinRange(), 1);

			return normalized;
		}

		private float GetNormalized(float value, float totalValue, float minValue, float rangeMin, float rangeMax)
		{
			// Normalised Formula
			// Normalised value between a and b
			//        
			//                       x - min x
			//     n = (b - a) -------------------- + a
			//                     max x - min x
			// 

			return (rangeMax - rangeMin) * ((value - minValue) / (totalValue - minValue)) + rangeMin;
		}
	}
}