﻿using System;
using System.Collections.Generic;

using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Unity.Common.Model;
using PlayGen.Orchestratror.Unity.Client;
using PlayGen.Unity.AsyncUtilities;
using UnityEngine;

namespace PlayGen.Orchestrator.PSL.Unity.Client
{
	// ReSharper disable once InconsistentNaming
	public class PSLOrchestrationClient : OrchestrationClient
	{
		[Serializable]
		private class SessionKeyValue
		{
			public string Key;

			public string Value;
		} 

		[SerializeField]
		private List<SessionKeyValue> _sessionIdentifiers = new List<SessionKeyValue>();

		private const string PlayerNameSessionKey = "playerNickName";
		private const string PlayerIdSessionKey = "playerID";
		private const string MatchIdSessionKey = "gameInstanceID";

		#region Overrides of OrchestrationClient
		

		protected override void OnInitialized()
		{
			base.OnInitialized();
			ClientImplementation.PlayerIdentified += ClientImplementation_PlayerIdentified;
		}

		protected void GetGameEndpoint(string playerId, string matchId, string loginToken)
		{
			LogProxy.Info($"{nameof(PSLOrchestrationClient)}::{nameof(GetGameEndpoint)} [{nameof(playerId)}: {playerId}, {nameof(matchId)}: {matchId}, {nameof(loginToken)}: {loginToken}]");
			var path = $"game/endpoint/{matchId}/{playerId}/{loginToken}";

			ClientImplementation.ServiceGET<EndpointResponse>(path, OnGetEndpointResponse, OnGetEndpointError);
		}

		private void OnGetEndpointError()
		{
			LogProxy.Error($"{nameof(PSLOrchestrationClient)}::{nameof(OnGetEndpointError)}");
			OnEndpointError(EndpointStatus.NoInfo);
		}

		private void OnGetEndpointResponse(EndpointResponse endpointResponse)
		{
			LogProxy.Info($"Endpoint received: {endpointResponse.host}:{endpointResponse.port}");
			OnEndpointLocated(endpointResponse);
		}

		private void ClientImplementation_PlayerIdentified(SessionIdentifier sessionIdentifier)
		{
			LogProxy.Info($"{nameof(PSLOrchestrationClient)}::{nameof(ClientImplementation_PlayerIdentified)}");
			GetGameEndpoint(sessionIdentifier.playerID, sessionIdentifier.gameInstanceID, sessionIdentifier.loginToken);
		}

		#endregion
	}
}
