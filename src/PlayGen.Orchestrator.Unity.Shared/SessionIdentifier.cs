namespace PlayGen.Orchestrator.Unity.Common.Model
{
	public class SessionIdentifier
	{
		public string playerNickName;

		public string playerID;

		public string gameInstanceID;

		public string loginToken;

		public string pslStoreUrl;

		public double localTimeOffset;
	}
}
