using System;
using System.Collections.Generic;
using System.Linq;
using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Contracts;
using PlayGen.Orchestrator.Unity.Common;
using PlayGen.Orchestrator.Unity.Common.Config;
using PlayGen.Orchestrator.Unity.Native;
using PlayGen.Orchestratror.Unity.Messages;
using PlayGen.Unity.AsyncUtilities;
using UnityEngine;
using UnityEngine.Networking;

namespace PlayGen.Orchestratror.Unity.Server
{
	public class OrchestratedGameServer : MonoBehaviour
	{
		#region inspector variables

		[SerializeField]
		private bool _useNetworkManager = true;

		[SerializeField]
		protected bool Standalone;

		#region editor debugging 

		[SerializeField]
		protected bool SimulatePlatformIntegration;

		[SerializeField]
		private string _commandLine;

		#endregion

		#region singleton 

		private static OrchestratedGameServer _instance;


		public void Awake()
		{
			if (_instance != null)
			{
				Destroy(gameObject);
			}
			else
			{
				_instance = this;
				_instance.Initialize();
				DontDestroyOnLoad(this);
			}
		}

		#endregion


		#endregion

		protected IOrchestrationClientImplementation ClientImplementation;

		public OrchestratedGameServerConfig Config { get; private set; }
		
		private Dictionary<string, RegisteredPlayer> _players;

		public GameState State { get; private set; }

		public Action ConfigValidated;

		public Action<GameRegistrationResponse> RegisteredWithOrchestrator;

		public Action<GameState> StateChanged;

		#region initialization

		protected virtual void Initialize()
		{
			try
			{
				if (Application.platform == RuntimePlatform.WebGLPlayer)
				{
					throw new InvalidOperationException("Server cannot run in WebGL");
				}

				_players = new Dictionary<string, RegisteredPlayer>();

				gameObject.AddComponent<AsyncConfigLoader>();
				gameObject.AddComponent<IntervalExecutor>();

				SetState(GameState.Initializing);
				_stateSetPending = true;

				AsyncConfigLoader.ReadConfig<OrchestratedGameServerConfig>("gameServer.config.json", OnConfigLoaded, OnError);
			}
			catch (Exception ex)
			{
				OnError(ex);
			}
		}

		private void OnError(Exception exception)
		{
			LogProxy.Error("Fatal error initializing OrchestratedGameServer");
			LogProxy.Exception(exception);
			Application.Quit();
		}

		private void SetInitialState()
		{
			var initialState = Config.StartMode == StartMode.Explicit
				? GameState.WaitingForStart
				: GameState.WaitingForPlayers;
			SetState(initialState);
			_stateSetPending = true;
		}

		private void OnConfigLoaded(OrchestratedGameServerConfig config)
		{
			try
			{
				Config = config;

				if (Standalone)
				{
					Config.GameInstanceId = Guid.NewGuid();
				}
				else
				{
					PopulateConfigFromStartupArgs();
				}
				if (ValidateConfig())
				{
					ConfigValidated?.Invoke();

					if (Standalone)
					{
						SetInitialState();
					}
					else
					{
						LogProxy.Debug("Initializing OrchestrationClient");

						gameObject.AddComponent<NativeOrchestrationClient>();
						ClientImplementation = gameObject.GetComponent<NativeOrchestrationClient>();
						ClientImplementation.Initialized += ClientImplementation_Initialized;
						ClientImplementation.Initialize(new OrchestrationClientConfig()
						{
							LogLevel = _instance.Config.LogLevel,
							ServiceUrl = _instance.Config.OrchestrationServerUrl,
							ServiceUrlSuffix = null,
						});
					}
				}
				else
				{
					throw new Exception("Invalid server configuration.");
				}
			}
			catch (Exception ex)
			{
				OnError(ex);
			}
		}

		private void PopulateConfigFromStartupArgs()
		{
			string[] args;
			if (SimulatePlatformIntegration)
			{
				LogProxy.Warning("Simulating - Environment.GetCommandLineArgs");
				args = _commandLine.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
			}
			else
			{
				args = Environment.GetCommandLineArgs();
			}

			var startupArguments = GameServerArguments.ParseArguments(args);

			Config.Host = startupArguments.Host;
			Config.Port = startupArguments.Port;
			Config.MaxPlayers = startupArguments.MaxPlayers;
			Config.StartMode = startupArguments.StartMode;
			Config.GameInstanceId = startupArguments.InstanceId;
		}

		public static void StartServer()
		{
			if (_instance.ValidateConfig())
			{
				_instance.StartListening();
			}
			else
			{
				throw new Exception("Invalid server configuration.");
			}
		}

		private bool ValidateConfig()
		{
			var valid = true;
			if (string.IsNullOrEmpty(Config.Host) || Config.Port == 0)
			{
				LogProxy.Error("Missing host configuration.");
				valid = false;
			}
			if (Config.MaxPlayers == 0 || Config.MinPlayers == 0 || Config.MinPlayers > Config.MaxPlayers)
			{
				LogProxy.Error("Invalid player configuration");
				valid = false;
			}
			if (Config.GameInstanceId == Guid.Empty)
			{
				LogProxy.Error("Invalid game instance id");
				valid = false;
			}
			if (string.IsNullOrEmpty(Config.OrchestrationServerUrl) && Standalone == false)
			{
				LogProxy.Error("Missing Orchestration server url");
				valid = false;
			}

			if (valid)
			{
				LogProxy.Info("Configuration validation success");
			}

			return valid;
		}

		#region orchestrated startup

		private void ClientImplementation_Initialized()
		{
			LogProxy.Debug("OrchestrationClient initialized.");
			RegisterWithOrchestrator();
		}

		private void RegisterWithOrchestrator()
		{
			var registrationRequest = new GameRegistrationRequest()
			{
				Id = Config.GameInstanceId.ToString(),
				State = State,
			};

			LogProxy.Info($"{nameof(OrchestratedGameServer)}::{nameof(RegisterWithOrchestrator)} [instanceId {registrationRequest.Id}, state: {registrationRequest.State}]");
			var path = "game/register";

			ClientImplementation.ServicePOST<GameRegistrationResponse>(path, registrationRequest, OnRegistered, () => throw new InvalidOperationException("Error registering game with orchestrator"));

		}

		private void OnRegistered(GameRegistrationResponse registrationResponse)
		{
			LogProxy.Info("Received GameRegistrationResponse");
			LogProxy.Info($"LessonPlan.Difficulty: {registrationResponse.difficulty}");
			LogProxy.Info($"LessonPlan.Language: {registrationResponse.language}");
			LogProxy.Info($"LessonPlan.MaxPlayers: {registrationResponse.maxPlayers}");
			LogProxy.Info($"LessonPlan.MaxGameTime: {registrationResponse.maxTime}");
			LogProxy.Info($"LessonPlan.Scenario: {registrationResponse.scenario}");

			// start polling for state changes
			IntervalExecutor.EnqueueIntervalTask(GetState, "GetState", 3);

			// set state according to startmode parameter
			SetInitialState();
			RegisteredWithOrchestrator?.Invoke(registrationResponse);
		}

		#endregion

		public virtual void StartListening()
		{
			try
			{
				if (_useNetworkManager)
				{
					DontDestroyOnLoad(gameObject);
					var networkManager = NetworkManager.singleton as OrchestratedNetworkManager;
					if (networkManager == null)
					{
						throw new InvalidOperationException($"NetworkManager is not {nameof(OrchestratedNetworkManager)}");
					}

					networkManager.GameServer = this;
					networkManager.networkAddress = Config.Host;
					networkManager.networkPort = Config.Port;
					networkManager.useWebSockets = true;
					networkManager.StartServer();

					//NetworkServer.RegisterHandler(MsgType.Connect, OnConnect);

					IntervalExecutor.EnqueueIntervalTask(Heartbeat, "Heartbeat", 1);

					LogProxy.Info($"NetworkManager WebSocket server started on {Config.Host}:{Config.Port}");
				}
				else
				{
					throw new NotImplementedException("Use network manager!");
				}
			}
			catch (Exception ex)
			{
				OnError(ex);
			}
		}


		private void Heartbeat()
		{
			LogProxy.Info("Sending Heartbeat");
			NetworkServer.SendToAll(500, new Heartbeat());
		}

		private bool _stateRequestPending;
		private bool _stateSetPending;
		private void GetState()
		{
			if (_stateSetPending)
			{
				UpdateState();
				return;
			}
			if (_stateRequestPending == false)
			{
				_stateRequestPending = true;
				LogProxy.Info($"{nameof(OrchestratedGameServer)}::{nameof(Heartbeat)} [instanceId {Config.GameInstanceId}, state: {State}]");
				var path = $"game/state/{Config.GameInstanceId}";
				ClientImplementation.ServiceGET<GameStateResponse>(path, OnStateResponse, () => throw new InvalidOperationException("Error updating state with orchestrator"));
			}
		}

		private void UpdateState()
		{
			if (_stateRequestPending == false)
			{
				_stateRequestPending = true;
				LogProxy.Info($"{nameof(OrchestratedGameServer)}::{nameof(Heartbeat)} [instanceId {Config.GameInstanceId}, state: {State}]");
				var path = "game/state";
				var stateRequest = new GameStateRequest()
										{
											id = Config.GameInstanceId.ToString(),
											state = State,
										};
				ClientImplementation.ServicePOST<GameStateResponse>(path, stateRequest, OnStateResponse, () => throw new InvalidOperationException("Error updating state with orchestrator"));
			}
		}

		private void OnStateResponse(GameStateResponse response)
		{
			LogProxy.Info($"GameStateResponse [state: {response.state}]");
			_stateRequestPending = false;
			_stateSetPending = false;
			SetState(response.state);
		}

		private void OnConnect(NetworkMessage netMsg)
		{
			LogProxy.Info("NetworkManager Connected");

		}

		public void SetState(GameState state, bool updateExternal = false)
		{
			if (state != State)
			{
				State = state;
				LogProxy.Info($"GameState set to {state}");
				OnStateChanged(state);
				if (updateExternal)
				{
					_stateSetPending = true;
				}
			}
		}

		protected virtual void OnStateChanged(GameState obj)
		{
			StateChanged?.Invoke(obj);
		}

		public void UpdateConnectedPlayers(List<string> players)
		{
			LogProxy.Info($"{nameof(OrchestratedGameServer)}::{nameof(UpdateConnectedPlayers)} [instanceId {Config.GameInstanceId}, players: {players.Count}]");
			var path = $"game/players/{Config.GameInstanceId}";
			var playerRequest = new GamePlayersRequest
			{
				PlayerIDs = players
			};
			ClientImplementation.ServicePOST(path, playerRequest, () => LogProxy.Info("Player list updated"), () => throw new InvalidOperationException("Error updating state with orchestrator"));
		}

		#endregion

		#region player management

		public void AddPlayer(NetworkConnection conn)
		{
			LogProxy.Info($"Player connected on connection {conn.connectionId}");
		}

		public void RemovePlayer(NetworkConnection conn)
		{
			LogProxy.Info($"Player disconnected on connection {conn.connectionId}");
		}

		#endregion

		private int _connections = -1;

		void Update()
		{
			var connections = NetworkServer.connections.Count(c => c != null);
			if (connections != _connections)
			{
				_connections = connections;
				LogProxy.Info($"Update: [ NetworkServer.connections: {connections}, SceneTime: {Time.timeSinceLevelLoad}]");
			}

		}


	}
}
