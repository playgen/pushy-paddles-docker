using PlayGen.Unity.AsyncUtilities;
using UnityEngine.Networking;

namespace PlayGen.Orchestratror.Unity.Server
{
	/// <summary>
	/// When using Master Server Kit together with UNET's Network Manager, we use a custom
	/// network manager in order to detect players connecting and disconnecting from the game
	/// server instead.
	/// </summary>
	public class OrchestratedNetworkManager : NetworkManager
	{
		/// <summary>
		/// Cached reference to the game server.
		/// </summary>
		public OrchestratedGameServer GameServer { get; set;  }

		public override void OnClientConnect(NetworkConnection conn)
		{
			LogProxy.Info($"{nameof(OrchestratedNetworkManager)} OnClientConnect");
			base.OnClientConnect(conn);
		}

		public override void OnClientError(NetworkConnection conn, int errorCode)
		{
			base.OnClientError(conn, errorCode);
			LogProxy.Error($"{GetType().Name}: ClientError {errorCode}");
		}

		public override void OnServerError(NetworkConnection conn, int errorCode)
		{
			base.OnServerError(conn, errorCode);
			LogProxy.Error($"{GetType().Name}: ServerError {errorCode}");

		}
		
		public override void OnDropConnection(bool success, string extendedInfo)
		{
			base.OnDropConnection(success, extendedInfo);
			LogProxy.Warning($"{GetType().Name}: DropConnection {extendedInfo}");
		}

		public override void OnServerConnect(NetworkConnection conn)
		{
			base.OnServerConnect(conn);
			LogProxy.Warning($"{GetType().Name}: OnServerConnect");
			GameServer.AddPlayer(conn);
		}

		public override void OnServerReady(NetworkConnection conn)
		{
			base.OnServerReady(conn);
			LogProxy.Warning($"{GetType().Name}: OnServerReady");
		}

		/// <summary>
		/// Called on the server when a client disconnects.
		/// </summary>
		/// <param name="conn">The disconnecting player's network connection.</param>
		public override void OnServerDisconnect(NetworkConnection conn)
		{
			base.OnServerDisconnect(conn);
			GameServer.RemovePlayer(conn);
		}
	}
}
