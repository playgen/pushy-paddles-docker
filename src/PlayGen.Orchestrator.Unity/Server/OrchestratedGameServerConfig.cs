﻿using System;
using PlayGen.Orchestrator.Common;
using UnityEngine;

namespace PlayGen.Orchestratror.Unity.Server
{
	[Serializable]
	public class OrchestratedGameServerConfig
	{
		public LogType LogLevel;

		public float HeartbeatInterval;

		public string Host;

		public int Port;

		public string OrchestrationServerUrl;

		public int MinPlayers;

		public int MaxPlayers;

		public StartMode StartMode;

		public Guid GameInstanceId;
	}
}
