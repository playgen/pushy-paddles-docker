using System;

namespace PlayGen.Orchestratror.Unity.Server
{
	// ReSharper disable once InconsistentNaming
	public class RegisteredPlayer
	{
		public Guid Id { get; set; }

		public string Nickname { get; set; }
	}
}
