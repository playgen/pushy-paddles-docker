﻿using System;
using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Unity.Common;
using PlayGen.Orchestrator.Unity.Common.Config;
using PlayGen.Orchestrator.Unity.Common.Model;
using PlayGen.Orchestrator.Unity.Native;
using PlayGen.Orchestrator.Unity.WebGL;
using PlayGen.Unity.AsyncUtilities;
using PlayGen.Unity.ASyncUtilities.WebGL;
using UnityEngine;
using UnityEngine.Networking;

namespace PlayGen.Orchestratror.Unity.Client
{
	public class OrchestrationClient : MonoBehaviour
	{
		#region editor debugging 

		[SerializeField]
		protected bool SimulatePlatformIntegration;

		[SerializeField]
		protected bool Standalone;

		#endregion

		#region singleton 

		private static OrchestrationClient _instance;

		protected GameServerArguments StartupArguments;
		
		public void Awake()
		{
			if (_instance != null)
			{
				Destroy(gameObject);
			}
			else
			{
				_instance = this;
				_instance.Initialize();
				DontDestroyOnLoad(this);
			}
		}

		#endregion

		#region state

		public static ClientState State => _instance?._state ?? ClientState.NotInitialized;

		private ClientState _state;

		private void SetState(ClientState state)
		{
			LogProxy.Info($"Client state transitioned to {state}");
			_state = state;
		}

		#endregion

		protected OrchestrationClientConfig Config;

		protected IOrchestrationClientImplementation ClientImplementation;
		private NetworkClient _client;

		public event Action<Endpoint> EndpointLocated;
		public event Action<EndpointStatus> EndpointError;
		public event Action<SessionIdentifier> PlayerIdentified;
		protected Endpoint Endpoint;

		#region initialization

		protected virtual void Initialize()
		{
			SetState(ClientState.Initializing);
			gameObject.AddComponent<AsyncConfigLoader>();
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				AsyncConfigLoader.Locator = new WebGLStreamingAssetsLocator();
			}
			AsyncConfigLoader.ReadConfig<OrchestrationClientConfig>("orchestrationClient.config.json", OnConfigLoaded, OnError);
		}

		private void OnError(Exception exception)
		{
			LogProxy.Error("Fatal error in OrchestrationClient");
			LogProxy.Exception(exception);
			Application.Quit();
		}

		private void OnConfigLoaded(OrchestrationClientConfig config)
		{
			Config = config;
			InitializeClientImplementation();
		}

		private void InitializeClientImplementation()
		{
			try
			{
				if (Application.platform == RuntimePlatform.WebGLPlayer)
				{
					gameObject.AddComponent<WebGLOrchestrationClient>();
					var webGlClient = gameObject.GetComponent<WebGLOrchestrationClient>();
					ClientImplementation = webGlClient;
				}
				else
				{ 
					gameObject.AddComponent<NativeOrchestrationClient>();
					ClientImplementation = gameObject.GetComponent<NativeOrchestrationClient>();
				}
				EndpointLocated += ClientImplementation.OnEndpointLocated;  // attach this handler as we need to call out to javascript to set the websocket route.
				EndpointError += ClientImplementation.OnEndpointLocationError;
				ClientImplementation.PlayerIdentified += PlayerIdentified;
				ClientImplementation.Initialized += ClientImplementation_Initialized;
				ClientImplementation.Initialize(Config);
			}
			catch (Exception ex)
			{
				OnError(ex);
			}
		}

		private void ClientImplementation_Initialized()
		{
			OnInitialized();
		}
		protected virtual void OnInitialized()
		{
			SetState(ClientState.Initialized);
		}

		public bool ConnectNetworkManager()
		{
			LogProxy.Info($"Endpoint not null: {Endpoint != null}");
			if (_client != null)
			{
				NetworkManager.singleton.StopClient();
				_client = null;
			}
			// these values will be ignored in the webgl mode as we intercept the websocket creation in javascript
			if (Endpoint != null)
			{
				NetworkManager.singleton.networkAddress = Endpoint.host;
				NetworkManager.singleton.networkPort = Endpoint.port;

				_client = NetworkManager.singleton.StartClient();

				_client.RegisterHandler(MsgType.Connect, OnConnect);
				_client.RegisterHandler(500, OnHeartbeat);
				return true;
			}
			else
			{
				LogProxy.Error("No server endpoint yet found.");
				return false;
			}
		}

		public static bool StartClient()
		{
			return _instance.ConnectNetworkManager();
		}

		private void OnConnect(NetworkMessage netMsg)
		{
			LogProxy.Info("NetworkManager Connected");
		}

		private void OnHeartbeat(NetworkMessage netMsg)
		{
			LogProxy.Info("Received Heartbeat");
		}


		#endregion

		protected void OnEndpointLocated(EndpointResponse endpointResponse)
		{
			var endpoint = new Endpoint
			{
				host = endpointResponse.host,
				port = endpointResponse.port,
				protocol = endpointResponse.protocol
			};
			var status = endpointResponse.status;
			if (status != EndpointStatus.Success)
			{
				OnEndpointError(status);
				OnError(new ArgumentOutOfRangeException($"Endpoint port not valid"));
				return;
			}
			if (endpoint.port == 0)
			{
				OnEndpointError(EndpointStatus.NoInfo);
				OnError(new ArgumentOutOfRangeException($"Endpoint port not valid"));
				return;
			}
			Endpoint = endpoint;
			LogProxy.Info($"Endpoint stored: {Endpoint.host}:{Endpoint.port}");
			EndpointLocated?.Invoke(endpoint);
		}

		protected void OnEndpointError(EndpointStatus status)
		{
			EndpointError?.Invoke(status);
		}
	}
}
