using PlayGen.Orchestrator.Common;

namespace PlayGen.Orchestrator.Contracts
{
	public class GameRegistrationRequest
	{
		public string Id;

		public GameState State;
	}
}
