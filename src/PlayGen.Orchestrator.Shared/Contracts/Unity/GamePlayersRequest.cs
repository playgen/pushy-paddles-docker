﻿using System.Collections.Generic;

namespace PlayGen.Orchestrator.Contracts
{
	public class GamePlayersRequest
	{
		public List<string> PlayerIDs;
	}
}
