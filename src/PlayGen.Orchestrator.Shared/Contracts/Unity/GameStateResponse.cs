﻿using PlayGen.Orchestrator.Common;

namespace PlayGen.Orchestrator.Contracts
{
	public class GameStateResponse
	{
		// ReSharper disable InconsistentNaming
		// lower case public fields to keep Unity JsonUtility happy on the client
		public string id;

		public GameState state;
		// ReSharper restore InconsistentNaming
	}
}
