namespace PlayGen.Orchestrator.Contracts
{
	public class LessonPlan
	{
		// ReSharper disable InconsistentNaming
		// lower case public fields to keep Unity JsonUtility happy on the client
		public string language;

		public int difficulty;

		public string scenario;

		public int maxPlayers;

		public int maxTime;
		// ReSharper restore InconsistentNaming
	}
}
