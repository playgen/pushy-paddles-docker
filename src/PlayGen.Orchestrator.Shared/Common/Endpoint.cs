namespace PlayGen.Orchestrator.Common
{
	public class Endpoint
	{
		public EndpointProtocol protocol;

		public string host;

		public int port;
	}
}
