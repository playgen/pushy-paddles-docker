using System;

namespace PlayGen.Orchestrator.Common
{
	public class GameServerArguments
	{
		private const string EndpointArg = "listen";
		private const string PlayerArg = "players";
		private const string StartModeArg = "start";
		private const string InstanceIdArg = "instanceId";

		public static string ArgumentFormat = $"{EndpointArg} {{0}}:{{1}} {PlayerArg} {{2}} {StartModeArg} {{3}} {InstanceIdArg} {{4}}";	// {0} host {1} port {2} players {3} startMode {4} instanceId

		public string Host { get; set; }

		public int Port { get; set; }

		public int MaxPlayers { get; set; }

		public StartMode StartMode { get; set; }

		public Guid InstanceId { get; set; }

		public static string GenerateArguments(string host, int port, int maxPlayers, StartMode startMode, Guid instanceId)
		{
			if (string.IsNullOrEmpty(host))
			{
				throw new ArgumentNullException(nameof(host));
			}
			if (port == 0)
			{
				throw new ArgumentOutOfRangeException(nameof(port));
			}
			if (maxPlayers == 0)
			{
				throw new ArgumentOutOfRangeException(nameof(maxPlayers));
			}
			if (instanceId == Guid.Empty)
			{
				throw new ArgumentOutOfRangeException(nameof(instanceId));
			}

			return string.Format(ArgumentFormat, host, port, maxPlayers, startMode, instanceId);
		}

		public static GameServerArguments ParseArguments(string[] args)
		{
			try
			{
				var arguments = new GameServerArguments();

				var foundPlayers = false;
				var foundEndpoint = false;
				var foundStartMode = false;

				for (var i = 0; i < args.Length; i++)
				{
					var arg = args[i];

					switch (arg)
					{
						case EndpointArg:
							var endpointValue = args[i + 1];

							var endpointFragments = endpointValue.Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries);
							if (endpointFragments.Length == 2)
							{
								var host = endpointFragments[0];
								var portString = endpointFragments[1];

								if (string.IsNullOrEmpty(host) == false && int.TryParse(portString, out var port))
								{
									foundEndpoint = true;
									arguments.Host = host;
									arguments.Port = port;
									i++;
									continue;
								}
							}
							throw new ArgumentException($"Invalid endpoint specification: {endpointValue}");

						case PlayerArg:
							var playerValue = args[i + 1];
							if (int.TryParse(playerValue, out var maxPlayers))
							{
								foundPlayers = true;
								arguments.MaxPlayers = maxPlayers;
								i++;
								continue;
							}
							throw new ArgumentException($"Invalid player specification: {playerValue}");

						case StartModeArg:
							var startValue = args[i + 1];

							if (string.IsNullOrEmpty(startValue) == false && EnumHelper.TryParse<StartMode>(startValue, out var startMode))
							{
								foundStartMode = true;
								arguments.StartMode = startMode;
								i++;
								continue;
							}
							throw new ArgumentException($"Invalid start mode specification: {startValue}");

						case InstanceIdArg:
							var instanceIdValue = args[i + 1];

							if (string.IsNullOrEmpty(instanceIdValue) == false)
							{
								arguments.InstanceId = new Guid(instanceIdValue);
								i++;
								continue;
							}
							throw new ArgumentException($"Invalid instanceId: {instanceIdValue}");
					}
				}

				if (foundPlayers && foundEndpoint && foundStartMode)
				{
					return arguments;
				}
				throw new ArgumentException("Madatory argument missing");
			}
			catch (Exception ex)
			{
				throw new ArgumentException("Could not parse command line arguments", ex);
			}
		}
	}
}
