﻿namespace PlayGen.Orchestrator.Common
{
	public enum GameState
	{
		Error = -1,
		NotInitialized = 0,
		Initializing = 1,
		WaitingForPlayers = 2,
		WaitingForStart = 3,
		Started = 4,
		Paused = 5,
		Stopped = 6,
	}
}
