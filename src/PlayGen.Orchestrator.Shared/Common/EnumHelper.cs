using System;

namespace PlayGen.Orchestrator.Common
{
	public static class EnumHelper
	{
		public static bool TryParse<TEnum>(string value, out TEnum result) where TEnum : struct
		{
			return TryParse(value, false, out result);
		}

		public static bool TryParse<TEnum>(string value, bool ignoreCase, out TEnum result) where TEnum : struct
		{
			var retValue = value != null && Enum.IsDefined(typeof(TEnum), value);
			result = retValue 
				? (TEnum)Enum.Parse(typeof(TEnum), value) 
				: default(TEnum);
			return retValue;
		}
	}
}
