﻿namespace PlayGen.Orchestrator.Common
{
    public class EndpointResponse
    {
        public EndpointProtocol protocol;

        public string host;

        public int port;

        public EndpointStatus status;

        public EndpointResponse(Endpoint e, EndpointStatus s)
        {
            protocol = e.protocol;
            host = e.host;
            port = e.port;
            status = s;
        }
    }
}
