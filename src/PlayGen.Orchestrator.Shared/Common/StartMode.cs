namespace PlayGen.Orchestrator.Common
{
	public enum StartMode
	{
		PlayersAvailable = 0,
		Explicit = 1,
	}
}
