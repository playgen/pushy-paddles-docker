namespace PlayGen.Orchestrator.Common
{
	public enum EndpointProtocol
	{
		UNETWebSocket,
		UNETWebSocketSecure,
		UNETUDP,
		UNETTCP,
	}
}
