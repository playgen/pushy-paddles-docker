﻿namespace PlayGen.Orchestrator.Common
{
    public enum EndpointStatus
    {
        NoInfo,
        Success,
        TokenFailure,
        MatchFailure,
        MatchFull,
        MatchAlreadyJoined,
    }
}
