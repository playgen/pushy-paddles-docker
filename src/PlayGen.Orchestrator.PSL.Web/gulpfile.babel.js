var gulp = require("gulp");
var jshint = require("gulp-jshint");
var concat = require("gulp-concat");
var inject = require("gulp-inject");
var eventStream = require("event-stream");
var del = require("del");
var angularTemplateCache = require("gulp-angular-templatecache");
var angularFileSort = require("gulp-angular-filesort");
var babel = require("gulp-babel");
var sourcemaps = require("gulp-sourcemaps");
var cssUseref = require("gulp-css-useref");
var gulpif = require("gulp-if");
var filter = require("gulp-filter");
var uglify = require("gulp-uglify");
var cleanCSS = require("gulp-clean-css");
var hash = require("gulp-hash-filename");
var rename = require("gulp-rename");

/*============================================
PROJECT CONFIGURATION   
============================================*/
var config = {
	index: "index.html",	
	development: {
		app: {
			root: "app",
			scripts: "app/**/*.js",
			styles: "app/**/*.css",
			templates: "app/**/*.html",
		},
		unity: {
			base: "../../Unity/Builds/Game/",
			content: [
				"../../Unity/Builds/Game/Build/**/*.*",
				"../../Unity/Builds/Game/StreamingAssets/**/*.*",
				"../../Unity/Builds/Game/TemplateData/**/*.*",
			],
		},
		vendor: {
			scripts: [
				"node_modules/jquery/dist/jquery.js",
				"node_modules/angular/angular.js",
				"node_modules/angular-ui-router/release/angular-ui-router.js",
				"node_modules/angular-animate/angular-animate.js",
				"node_modules/angular-aria/angular-aria.js",
				"node_modules/angular-material/angular-material.js",
				"node_modules/angular-messages/angular-messages.js",
				"node_modules/angular-cookies/angular-cookies.js",

				"../../PSL_JSClientAPI/pslLibs/**/*.js",
			],
			styles: [
				"node_modules/angular-material/angular-material.css",
				"node_modules/material-design-icons/iconfont/material-icons.css"
			]
		},
		ignoreSuffixes: [".production.", ".tests."],
		sourcemaps: true,
		bundle: false,
		minifyScripts: false,
		minifyCss: false,
		hash: false,
		output: {
			root: "../PlayGen.Orchestrator.PSL.Service/wwwroot",
			app: "../PlayGen.Orchestrator.PSL.Service/wwwroot/app",
			vendor: "../PlayGen.Orchestrator.PSL.Service/wwwroot/vendor",
			unity: "../PlayGen.Orchestrator.PSL.Service/wwwroot/unity"
		}		
	},
	production: {
		app: {
			root: "app",
			scripts: "app/**/*.js",
			styles: "app/**/*.css",
			templates: "app/**/*.html",
		},
		unity: {
			base: "../../Unity/Builds/Game/",
			content: [
				"../../Unity/Builds/Game/Build/**/*.*",
				"../../Unity/Builds/Game/StreamingAssets/**/*.*",
				"../../Unity/Builds/Game/TemplateData/**/*.*",
			],
		},
		vendor: {
			scripts: [
				"node_modules/jquery/dist/jquery.js",
				"node_modules/angular/angular.js",
				"node_modules/angular-ui-router/release/angular-ui-router.js",
				"node_modules/angular-animate/angular-animate.js",
				"node_modules/angular-aria/angular-aria.js",
				"node_modules/angular-material/angular-material.js",
				"node_modules/angular-messages/angular-messages.js",
				"node_modules/angular-cookies/angular-cookies.js",

				"../../PSL_JSClientAPI/pslLibs/**/*.js",
			],
			styles: [
				"node_modules/angular-material/angular-material.css",
				"node_modules/material-design-icons/iconfont/material-icons.css"
			]
		},
		ignoreSuffixes: [".development.", ".tests."],
		sourcemaps: false,
		bundle: false,
		minifyScripts: false,
		minifyCss: false,
		hash: false,
		output: {
			root: "../PlayGen.Orchestrator.PSL.Service/wwwroot",
			app: "../PlayGen.Orchestrator.PSL.Service/wwwroot/app",
			vendor: "../PlayGen.Orchestrator.PSL.Service/wwwroot/vendor",
			unity: "../PlayGen.Orchestrator.PSL.Service/wwwroot/unity"
		}
	},
	build: {
		hashFormat: "{name}.{hash}.{ext}",
		babelPreset: "es2015"	
	},
	devTools: {
		esversion: 6
	}
};

/*============================================
ACTIVE CONFIG
============================================*/
var activeConfig = null;

function setDevelopment(done) {
	activeConfig = config.development;
	done();
}

function setProduction(done) {
	activeConfig = config.production;
	done();
}

/*============================================
STREAMS
============================================*/
function clean() {
	var paths = Array.from(activeConfig.output, output => output + "/*");
	return del(paths, { force: true });
}

function validateJs() {
	return gulp.src(activeConfig.app.scripts)
		.pipe(jshint({esversion: config.devTools.esversion}))
		.pipe(jshint.reporter("jshint-stylish"));
};

function appScripts() {
	var paths = [activeConfig.app.scripts];
	activeConfig.ignoreSuffixes.forEach(ignoreSuffix => {
		var ignorePath = "!" + activeConfig.app.root + "/**" + ignoreSuffix + "**";
		paths.push(ignorePath);
	});

	return gulp.src(paths)
		.pipe(gulpif(activeConfig.sourcemaps, sourcemaps.init()))
		.pipe(babel({ // compiles ecma6 code to a version compatable with browsers and the angularFileSort plugin
			presets: [config.build.babelPreset]
		}));
};

// combine the angular template html files into one javascript blob
function templates() {
	return gulp.src([
			activeConfig.app.templates,
			"!" + activeConfig.app.root + "/**" + activeConfig.ignoreSuffix + "**"
		])
		.pipe(angularTemplateCache({ standalone: true }));
};

/*============================================
OUTPUT STREAMS
============================================*/
function generateIndex() {
	console.log("Generating index");
	return modifyIndex(config.index, { all: true });
}

function updateIndex(sections) {
	console.log("Updating index: ");
	return modifyIndex(activeConfig.output.root + "/index.html", sections);
}

function modifyIndex(index, sections) {
	return gulp.src(index)		
		.pipe(gulpif(sections.all || sections.vendorStyles, inject(vendorStylesOutput(), { name: "vendor", ignorePath: activeConfig.output.root, relative: true })))
		.pipe(gulpif(sections.all || sections.vendorScripts, inject(vendorScriptsOutput(), { name: "vendor", ignorePath: activeConfig.output.root, relative: true})))
		.pipe(gulpif(sections.all || sections.srcStyles, inject(appStylesOutput(), { name: "src", ignorePath: activeConfig.output.root, relative: true})))				
		.pipe(gulpif(sections.all || sections.srcScripts, inject(srcScriptsOutput(), { name: "src", ignorePath: activeConfig.output.root, relative: true})))
		.pipe(gulp.dest(activeConfig.output.root));
}

function allStylesOutput() {
	return eventStream.merge(
		vendorStylesOutput(),
		appStylesOutput()
	);
}

function srcScriptsOutput() {
	return eventStream.merge(
			appScripts(),
			templates()
		)				
		.pipe(angularFileSort())		// puts files in correct order to satisfy angular dependency injection		
		.pipe(gulpif(activeConfig.bundle, concat("app.js")))				
		.pipe(gulpif(activeConfig.hash, hash({ "format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyScripts, uglify()))
		.pipe(gulpif(activeConfig.minifyScripts, rename(path => path.basename += ".min")))
		.pipe(gulpif(activeConfig.sourcemaps, sourcemaps.write('.')))	// write sourcemaps for processed files
		
		.pipe(gulp.dest(activeConfig.output.app));
}

function appStylesOutput() {
	var cssFilter = filter("**/*.css", {restore: true});

	return gulp.src(activeConfig.app.styles)						
		.pipe(cssUseref({base: "assets"}))	// copies referenced files (fonts/images) within the css file		
		
		// filter to apply transformations only to .css files
		.pipe(cssFilter)
		.pipe(gulpif(activeConfig.bundle, concat("app.css")))		
		.pipe(gulpif(activeConfig.hash, hash({ "format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyCss, cleanCSS()))
		.pipe(gulpif(activeConfig.minifyCss, rename(path => path.basename += ".min")))
		.pipe(cssFilter.restore)

		.pipe(gulp.dest(activeConfig.output.app));
};

function vendorScriptsOutput() {
	return gulp.src(activeConfig.vendor.scripts)				
		.pipe(gulpif(activeConfig.bundle, concat("vendor.js")))
		.pipe(gulpif(activeConfig.hash, hash({ "format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyScripts, uglify()))
		.pipe(gulpif(activeConfig.minifyScripts, rename(path => path.basename += ".min")))
		.pipe(gulp.dest(activeConfig.output.vendor));
}

function vendorStylesOutput() {
	var cssFilter = filter("**/*.css", {restore: true});

	return gulp.src(activeConfig.vendor.styles)		
		.pipe(cssUseref({base: "assets"}))	// copies referenced files (fonts/images) within the css file		

		// filter to apply transformations only to .css files
		.pipe(cssFilter)	
		.pipe(gulpif(activeConfig.bundle, concat("vendor.css")))	
		.pipe(gulpif(activeConfig.hash, hash({ "format": config.build.hashFormat})))			
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyCss, cleanCSS()))	
		.pipe(gulpif(activeConfig.minifyCss, rename(path => path.basename += ".min")))	
		.pipe(cssFilter.restore)	

		.pipe(gulp.dest(activeConfig.output.vendor));
};

function unityContent() {
	return gulp.src(activeConfig.unity.content, { base: activeConfig.unity.base })
		.pipe(gulp.dest(activeConfig.output.unity));
}

/*============================================
TASKS
============================================*/
gulp.task(clean);

gulp.task(validateJs);

gulp.task("build-development", gulp.series(setDevelopment, clean, validateJs, unityContent, generateIndex));

gulp.task("build-production", gulp.series(setProduction, clean, validateJs, unityContent, generateIndex));

gulp.task("watch-development", function() {
	gulp.watch(config.index, gulp.series("build-development"));
	gulp.watch(config.development.vendor.styles, gulp.series("build-development"));
	gulp.watch(config.development.vendor.scripts, gulp.series("build-development"));
	gulp.watch(config.app.styles, gulp.series("build-development"));
	gulp.watch(config.app.scripts, gulp.series("build-development"));
	gulp.watch(config.app.templates, gulp.series("build-development"));	
	gulp.watch(config.unity.content, gulp.series("build-development"));
});

gulp.task("default", gulp.series("build-development"));
gulp.task("development", gulp.series("build-development", "watch-development"));
gulp.task("production", gulp.series("build-production"));