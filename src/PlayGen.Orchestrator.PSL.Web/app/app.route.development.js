angular
	.module("pslOrchestrator")
	.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("lessons", {
				url: "/",
				component: "pageLessonList"
			})
			.state("gameDev", {
				url: "/game/",
				component: "pageUnityGameDev"
			});
			$urlRouterProvider.otherwise("/");
	}]);