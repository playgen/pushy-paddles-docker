angular
	.module("pslOrchestrator")
	.component("pslStatus", {
		templateUrl: "components/pslStatus/psl-status.development.html",
		bindings: {},
		controller: [
			"$http", "PslConfig", "$interval",
			function($http, pslConfig, $interval) {
				var ctrl = this;

				var state = -1;

				var interval = 3000;

				var lastRequest = 0;

				// public enum GameState
				// {
				// 	Error = -1,
				// 	NotInitialized = 0,
				// 	Initializing = 1,
				// 	WaitingForPlayers = 2,
				// 	WaitingForStart = 3,
				// 	Started = 4,
				// 	Paused = 5,
				// 	Stopped = 6,
				// }


				ctrl.setState = function (stateInt) {
					var gameInstanceId = window.PlayGen.PSL.GameInfo.gameInstanceID;
					var state = { id: gameInstanceId, state: stateInt };
					$http.post(pslConfig.ApiBase + "game/state", state);
				};

				ctrl.startDisabled = function() {
					return state <= 1 || state == 4 || state == 6;
				};

				ctrl.pauseDisabled = function() {
					return state != 4;
				};

				ctrl.stopDisabled = function() {
					return state < 2 || state == 6;
				};

				function getState() {
					var now = Date.now();
					var elapsed = now - lastRequest;

					if (elapsed > interval) {
						lastRequest = now;

						var gameInstanceId = window.PlayGen.PSL.GameInfo.gameInstanceID;
						if (gameInstanceId) {

							$http.get(pslConfig.ApiBase + "game/state/" + gameInstanceId)
								.then(stateResponse => {
									state = stateResponse.data.state;
									getState();
								}, 
								errorResponse => {
									console.error("Error getting state: " + errorResponse.message);
									getState();
								});

						}
					}
					else {
						var delay = interval - elapsed;
						$interval(getState, delay, 1);
					}
				}

				function initStatusLoop() {
					getState();
				}

				ctrl.$onInit = function() {
					if (window.PlayGen.PSL.Initialized) {
						initStatusLoop();
					}
					else {
						window.PlayGen.PSL.OnInitialized.push(initStatusLoop);
					}
				};

				ctrl.$onDestroy = function() {
					$interval.cancel(interval);
				};
			}
		]
	});