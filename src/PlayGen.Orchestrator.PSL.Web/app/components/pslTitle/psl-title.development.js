angular
	.module("pslOrchestrator")
	.component("pslTitle", {
		templateUrl: "components/pslTitle/psl-title.development.html",
		bindings: {
			default: "@",
			prefix: "@",
			suffix: "@",
		},
		controller: ["PslTest",
			function(pslTest) {
				var ctrl = this;

				function setTitle(value) {
					return (ctrl.prefix || "") + " " + (value || "") + " " + (ctrl.suffix || "");
				}

				ctrl.title = setTitle(ctrl.default);

				ctrl.$onInit = function() {
					pslTest.GetGameName().then(name => {
						ctrl.title = setTitle(name);
					});
				};

			}
		]
	});