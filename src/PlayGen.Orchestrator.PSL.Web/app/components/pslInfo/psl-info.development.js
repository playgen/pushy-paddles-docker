angular
	.module("pslOrchestrator")
	.component("pslInfo", {
		templateUrl: "components/pslInfo/psl-info.development.html",
		bindings: {},
		controller: ["$interval",
			function($interval) {
				var ctrl = this;

				ctrl.items = [];

				function getGameInfo() {
					var gameInfo = window.PlayGen.PSL.PSLClient.getGameInfo();
					ctrl.items = Object.getOwnPropertyNames(gameInfo)
						.map(function(prop) {
							return { 
								key: prop,
								value: gameInfo[prop],
							};
						});
				}

				function onPslInitialized() {
					console.log("PSL Game Info Loaded: " + window.PlayGen.PSL.GameInfo);
					getGameInfo();
					$interval(getGameInfo, 5000);
				}

				ctrl.$onInit = function() {
					window.PlayGen.PSL.OnInitialized.push(onPslInitialized);
				};

			}
		]
	});