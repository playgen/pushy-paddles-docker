angular
	.module("pslOrchestrator", [
		"templates", 
		"ui.router", 
		"ngMaterial",
		"ngCookies"
	]);