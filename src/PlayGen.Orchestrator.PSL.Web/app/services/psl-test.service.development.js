angular
	.module("pslOrchestrator")
	.factory("PslTest", 
		[
			"PslConfig",
			"$http",
			"$cookies",
			"$q",
			function(pslConfig,
				$http,
				$cookies,
				$q) {

				var service = {};

				service.GetLessons = function() {
					return $http.get(pslConfig.ApiBase + "platform/lessons")
						.then(lessonsResponse => {
							return lessonsResponse.data;
						}, 
						errorResponse => {

						});
				};

				service.TestCookies = function() {
					var nick = $cookies.get("nick");
					var studentId = $cookies.get("studentID");
					var gameId = $cookies.get("gameID");
					var loginToken = $cookies.get("loginToken");
					var pslStoreUrl = $cookies.get("pslStoreURL");

					return !!(nick && studentId && gameId && loginToken && pslStoreUrl);
				};

				service.SetCookies = function (gameId, studentId, nick) {
					$cookies.put("nick", nick);
					$cookies.put("studentID", studentId);
					$cookies.put("gameID", gameId);
					$cookies.put("loginToken", "TestUserToken");
					$cookies.put("pslStoreURL", "localhost:49769");

					return nick && studentId && gameId;
				};



				service.GetGameName = function() {
					var deferred  = $q.defer();

					$http.get("actions/getLessonPlanTemplate")
						.then(lptResponse => {
							var lpt = lptResponse.data;

							deferred.resolve(lpt['Name']);
						})

					return deferred.promise;
				}

				return service;
			}
		]);