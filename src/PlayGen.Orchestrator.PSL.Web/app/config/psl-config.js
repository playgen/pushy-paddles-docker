angular
	.module("pslOrchestrator")
	.value("PslConfig", {
		ApiBase: "/api/",
		UsePSLVideo: false,
		UsePSLVoice: false,
	});