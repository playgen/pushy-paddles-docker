angular
	.module("pslOrchestrator")
	.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("game", {
				url: "/",
				component: "pageUnityGame"
			});
			$urlRouterProvider.otherwise("/");
	}]);