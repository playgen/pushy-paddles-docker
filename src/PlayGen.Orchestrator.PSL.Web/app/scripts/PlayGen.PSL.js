(function () {
	// create some global objects if the do not exist already
	// some of these methods will be called by unity, some by angular
	if (!window.PlayGen) {
		window.PlayGen = {};
	}
	if (!window.PlayGen.PSL) {
		// set configuration values, functions will be injected later
		window.PlayGen.PSL = {
			Initialized: false,
			OnInitialized: []
		};
	}

	// instantiate the PsL client API
	window.PlayGen.PSL.PSLClient = new PSLClient();

	// callback from the client initialize method
	function onAPIInitialised(result) {
		console.log("[PSL] API Initialized");
		// get the users token
		window.PlayGen.PSL.GameInfo = window.PlayGen.PSL.PSLClient.getGameInfo();
		// execute registered callbacks
		for(var i = 0; i < window.PlayGen.PSL.OnInitialized.length; i++) {
			var cb = window.PlayGen.PSL.OnInitialized[i];
			if (typeof(cb) === 'function') {
				cb();
			}
		}	
		window.PlayGen.PSL.Initialized = true;
	}

	// callback when face tracking data is generated - this will produce a lot of noise so only log if necessary
	function onFaceData(data) {
		//console.debug("[PSL] OnFaceData");
	}

	// callback when voice tracking data is generated - this will produce a lot of noise so only log if necessary
	function onVoiceData(data) {
		//console.debug("[PSL] OnVoiceData");
	}

	// initialize the PSL API
	// currently this is called after the unity loader has bootstrapped and the game is awakenign
	window.PlayGen.PSL.Initialize = function() {
		try {
			// get the configuration object from the angular app
			var config = angular.element('body').injector().get('PslConfig');
			var voiceConfig = config.UsePSLVoice ? { trackingCallback : onVoiceData } : null;
			// video and canvas ids must match those set in html
			var videoConfig = config.UsePSLVideo
				? {
					videoID : 'pslVideoInput',
					canvasID : 'pslVideoOutput',
					width: 320,
					height: 240,
					trackingCallback : onFaceData
				}
				: null;
			window.PlayGen.PSL.PSLClient.initialise(voiceConfig, videoConfig, onAPIInitialised);
		}
		catch(exception) {
			console.error(exception);
		}
	};

	// asyncronously get the PSL token values and send them to unity
	window.PlayGen.PSL.SetGameInfo = function() {

		function sendGameInfo() {
			// this didnt seem to behave well when being triggered by the same execution path that called out of unity
			// schedule a timeout to make it call back after the previous has returned
			setTimeout(function() {
				var gameInfo = JSON.stringify(window.PlayGen.PSL.PSLClient.getGameInfo());
				console.log("[DEBUG] gameInfo: " + gameInfo);
				window.PlayGen.GameInstance.SendMessage(window.PlayGen.UnityProxy.RootObjectName,
					"OnPlayerIdentifier",
					gameInfo);			
			}, 0);
		}
		if (window.PlayGen.PSL.Initialized) {
			// if the api is initialized we can do this immediately
			sendGameInfo();			
		}
		else {
			// else enqueue a callback for onAPIInitialized
			window.PlayGen.PSL.OnInitialized.push(sendGameInfo);
		}
	};
})();
