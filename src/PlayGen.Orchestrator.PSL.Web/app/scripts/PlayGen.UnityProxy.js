(function () {
	// create some global objects if the do not exist already
	// some of these methods will be called by unity, some by angular
	if (!window.PlayGen) {
		window.PlayGen = {};
	}
	if (!window.PlayGen.UnityProxy) {
		// set configuration values, functions will be injected later
		window.PlayGen.UnityProxy = {
			Config: {
				StreamingAssetsPath: "unity/StreamingAssets/"
			}
		};
	}

	// called as the WebGL interaction components are loaded in Unity
	window.PlayGen.UnityProxy.OnInitializing = function (rootObjectName) {
		// store the root object name so that sendmessage calls can locate the target
		window.PlayGen.UnityProxy.RootObjectName = rootObjectName;
		console.log("[UNITYPROXY] WebGL client implementation initialized on GameObject " + rootObjectName);

		// initialize the PSL API wrapper
		window.PlayGen.PSL.Initialize();

		// handle the window unloading event
		function unload() {
			console.debug("[UNITYPROXY] Window Unloading");
			window.PlayGen.UnityProxy.BeforeWindowUnload();
			if (service) {
				service.EndSession();
			}
		}
		window.addEventListener('beforeunload', unload);
	};

	// called when the interaction component has loaded
	window.PlayGen.UnityProxy.Initialized = function() {
		console.log("[UNITYPROXY] Initialized");
		// signal the PSL wrapper to callback with game instance details
		window.onresize();
		window.PlayGen.PSL.SetGameInfo();
	};

	// called by the BeforeWindowUnload DOM event
	// this cannot be relied upon to complete before the application is unloaded completely, so do anything important like flushing queues preemptively
	window.PlayGen.UnityProxy.BeforeWindowUnload = function() {
		// call into unity in case it needs to do any cleanup / shutdown
		window.PlayGen.GameInstance.SendMessage(window.PlayGen.UnityProxy.RootObjectName, 
			'OnWindowBeforeUnload', 
			'');
		// shutdown the PSL client
		window.PlayGen.PSL.PSLClient.shutdown();
	};

	// unity cannot determine the web sites root path reliably, we can do a better job
	window.PlayGen.UnityProxy.GetApplicationBase = function() {
		// if there is a base element in the page then trust the developer knows what they are doing and return that
		var base = document.getElementsByTagName('base');
		if (base.length == 1)
		{
			return base[0].href;
		} 
		// otherwise, try and strip out any #ref and query string components from the current location
		var value = window.location.pathname.substr(0, window.location.pathname.lastIndexOf("/") + 1);
		console.log("[UNITYPROXY] GetApplicationBase: " + value);
		return value;
	};

	// unity webgl player is not compatible with secure websockets or websockets that exist at a path relative to the server base by default
	// the UNETWebSockets.jspre plugin overrides the default implementation of opening the socket with an interceptor that will read the url from the window.UNETWebSocketURL variable if set
	window.PlayGen.UnityProxy.SetWebSocketURL = function(port) {
		// compose a url that looks like ws(s)://host/xxx/wss/<port>
		// the reverse proxy configuration will forward this to ws://internal_host:port
		window.UNETWebSocketURL = "ws" + (location.protocol == "https:" ? "s" : "") + "://" + location.host + location.pathname.replace(location.search, "")
			.substr(0, location.pathname.replace(location.search, "").lastIndexOf("/") + 1) + "wss/" + port;	
		console.log("[UNITYPROXY]UNETWebSocketURL set to " + window.UNETWebSocketURL);
	};

	// allow unity to signal the wrapper that the endpoint location failed and it should handle any error conditions
	window.PlayGen.UnityProxy.OnEndpointLocationError = function() {
		console.log("[UNITYPROXY] OnEndpointLocationError (GameId is probably no longer valid - are you refreshing the browser?)");
	};
})();
