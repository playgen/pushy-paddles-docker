angular
	.module("pslOrchestrator")
	.component("pageUnityGame", {
		templateUrl: "pages/unityGame/page-unityGame.production.html",
		controller: [
			"$state", 
			function($state) 
			{
				var ctrl = this;

				function resize() {
					var newSize = canvasSize();

					window.PlayGen.GameInstance.container.width = newSize[0];
					window.PlayGen.GameInstance.container.height = newSize[1];
					window.PlayGen.GameInstance.container.firstChild.width = newSize[0];
					window.PlayGen.GameInstance.container.firstChild.height = newSize[1];
				}

				function canvasSize() {
					var arx = 16;
					var ary = 9;
					var padx = 8;
					var pady = 8;

					var width = window.innerWidth;
					var height = window.innerHeight - 64;
					var unit = Math.min(Math.floor((width - padx) / arx), Math.floor((height - pady) / ary));
					var x = arx * unit;
					var y = ary * unit;
					return [x, y];
				}


				ctrl.$onInit = function() {
					window.onresize = resize;
					var newSize = canvasSize();
					window.PlayGen.GameInstance = UnityLoader.instantiate("gameContainer", "unity/Build/Game.json", { width: newSize[0], height: newSize[1], onProgress: UnityProgress });
				};

				ctrl.TrackingDisabled = function() {
					return !window.PlayGen.PSL.Initialized;
				};

				ctrl.trackFace = false;
				ctrl.toggleTrackFace = function() {
					if (window.PlayGen.PSL.Initialized) {
						ctrl.trackFace = !ctrl.trackFace;
						ctrl.showFace = ctrl.trackFace;
						window.PlayGen.PSL.PSLClient.trackFace(ctrl.trackFace);
						console.log("PSL Face tracking " + (ctrl.trackFace ? "started" : "stopped"));
					}
				};

				ctrl.trackVoice = false;
				ctrl.toggleTrackVoice = function() {
					if (window.PlayGen.PSL.Initialized) {
						ctrl.trackVoice = !ctrl.trackVoice;
						window.PlayGen.PSL.PSLClient.trackVoice(ctrl.trackVoice);
						console.log("PSL Voice tracking " + (ctrl.trackVoice ? "started" : "stopped"));
					}
				};

				ctrl.showFace = false;
				ctrl.toggleShowFace = function() {
					ctrl.showFace = !ctrl.showFace;
				};
			}
		]
	});