angular
	.module("pslOrchestrator")
	.component("pageLessonList", {
		templateUrl: "pages/lessonList/page-lessonList.development.html",
		controller: [
			"$state",
			"PslTest", 
			function($state,
				PslTest) 
			{
				var ctrl = this;

				PslTest.GetLessons()
					.then(lessons => {
						ctrl.lessonList = lessons;
					});

				ctrl.SelectIdentity = function (groupId, playerId, name) {
					PslTest.SetCookies(groupId, playerId, name);
					if(PslTest.TestCookies()) {
						$state.go("gameDev");
					}
				};
			}
		]
	});