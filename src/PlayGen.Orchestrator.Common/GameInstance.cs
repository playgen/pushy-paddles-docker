﻿using System.Threading.Tasks;
using PlayGen.Orchestrator.Contracts;

namespace PlayGen.Orchestrator.Common
{
	public class GameInstance
	{
		public GameIdentifier GameIdentifier { get; }

		public PlayerIdentifier[] Players { get; set; }

		public Endpoint Endpoint { get; set; }

		public bool StateChangePending { get; set; }

		public GameState RequestedState { get; set; }

		public GameState State { get; set; }

		public TaskCompletionSource<Endpoint> EndpointCompletionSource { get; }

		public int ProcessId { get; set; }
		
		public LessonPlan LessonPlan { get; set; }

		public GameInstance(GameIdentifier gameId)
		{
			GameIdentifier = gameId;
			EndpointCompletionSource = new TaskCompletionSource<Endpoint>();
		}
	}

	#region conversion

	public static class GameInstanceExtensions
	{
		public static Endpoint GetEndpoint(this GameInstance gameInstance)
		{
			return gameInstance.Endpoint;
		}
	}

	#endregion
}
