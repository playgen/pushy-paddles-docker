﻿using System;

namespace PlayGen.Orchestrator.Common
{
	public struct GameIdentifier : IEquatable<GameIdentifier>
	{
		public string Name { get; }

		public Guid Id { get; }

		public GameIdentifier(string guid, string name)
		{
			Id = Guid.Parse(guid);
			Name = name;
		}

		public GameIdentifier(Guid id, string name)
		{
			Id = id;
			Name = name;
		}

		#region Equality members

		public bool Equals(GameIdentifier other)
		{
			return Id.Equals(other.Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is GameIdentifier && Equals((GameIdentifier) obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		#endregion
	}
}
