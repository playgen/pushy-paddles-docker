﻿namespace PlayGen.Orchestrator.Common
{
	public interface IRelativePathLocator
	{
		string GetAbsoluteFromContentRelativePath(string relative);
	}
}