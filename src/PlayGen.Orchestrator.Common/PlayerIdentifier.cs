﻿using System;

namespace PlayGen.Orchestrator.Common
{
	public class PlayerIdentifier : IEquatable<PlayerIdentifier>
	{
		public Guid Id { get; }

		public bool Connected { get; private set; }

		public PlayerIdentifier(string guid)
		{
			Id = Guid.Parse(guid);
			Connected = false;
		}

		public PlayerIdentifier(Guid id)
		{
			Id = id;
			Connected = false;
		}

		public void SetConnectionState(bool connection)
		{
			Connected = connection;
		}

		#region Equality members

		public bool Equals(PlayerIdentifier other)
		{
			return Id.Equals(other.Id) && Connected.Equals(other.Connected);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			var a = obj as PlayerIdentifier;
			return a != null && Equals(a);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		#endregion
	}
}
