using System;
using System.Linq;
using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.PSL.Common;
using PlayGen.Orchestrator.Contracts;

namespace PlayGen.Orchestrator.PSL.Model
{
	public class Match : LessonGroup, IEquatable<Match>
	{
		public Guid LessonId { get; set; }

		public LessonPlan LessonPlan { get; set; }
		
		public GameState State { get; set; }

		#region Equality members

		public bool Equals(Match other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return base.Equals(other) && LessonId.Equals(other.LessonId);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((Match) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (base.GetHashCode() * 397) ^ LessonId.GetHashCode();
			}
		}

		#endregion
	}

	#region Conversion

	public static class MatchExtenstions
	{
		public static PSLGameInstance ToGameInstance(this Match match)
		{
			return new PSLGameInstance(new GameIdentifier(match.Id, match.Name))
			{
				Players = match.PlayerIds.Select(p => new PlayerIdentifier(p)).ToArray(),
				LessonPlan = match.LessonPlan,
			};
		}
	}

	#endregion
}
