using System;

namespace PlayGen.Orchestrator.PSL.Model
{
	public class Lesson : IEquatable<Lesson>
	{
		public Guid Id { get; set; }

		public LessonGroup[] Groups { get; set; }

		#region Equality members

		public bool Equals(Lesson other)
		{
			if (ReferenceEquals(null, other)) return false;
			return ReferenceEquals(this, other) || Id.Equals(other.Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((Lesson) obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		#endregion
	}
}
