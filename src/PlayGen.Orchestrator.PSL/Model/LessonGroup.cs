using System;

namespace PlayGen.Orchestrator.PSL.Model
{
	public class LessonGroup : IEquatable<LessonGroup>
	{
		public string Name { get; set; }

		public Guid Id { get; set; }

		public Guid[] PlayerIds { get; set; }

		#region Equality members

		public bool Equals(LessonGroup other)
		{
			if (ReferenceEquals(null, other)) return false;
			return ReferenceEquals(this, other) || Id.Equals(other.Id);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((LessonGroup) obj);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		#endregion
	}
}
