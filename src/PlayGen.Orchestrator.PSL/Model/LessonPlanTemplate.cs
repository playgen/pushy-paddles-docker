namespace PlayGen.Orchestrator.PSL.Model
{
	public class LessonPlanTemplate
	{
		#region serialization subclasses

		public class LessonPlanTemplateTeam
		{
			public int Count { get; set; }
		}

		public class LessonPlanTemplateGroup
		{
			public int Name { get; set; }

			public LessonPlanTemplateTeam[] Teams { get; set; }
		}

		public class LessonPlanTemplateAge
		{
			public int Min { get; set; }

			public int Max { get; set; }
		}

		public class LessonPlanTemplateCountry
		{
			public string Name { get; set; }
			public string Code { get; set; }

			public LessonPlanTemplateAge Age { get; set; }
		}

		public class LessonPlanTemplateLanguage
		{
			public string Name { get; set; }
		}

		public class LessonPlanTemplateDifficulty
		{
			public string Name { get; set; }

			public int Code { get; set; }
		}

		public class LessonPlanTemplateScenario
		{
			public string Name { get; set; }
		}

		public class LessonPlanTemplateParameterRange
		{
			public int Min { get; set; }

			public int Max { get; set; }

			public int Step { get; set; }
		}

		public class LessonPlanTemplateParameter
		{
			public string Key { get; set; }

			public string Description { get; set; }

			public string ValueType { get; set; }

			public object[] Values { get; set; }

			public LessonPlanTemplateParameterRange Range { get; set; }

			public bool Required { get; set; }

			public object Default { get; set; }
		}

		#endregion

		public string Name { get; set; }

		public string Instructions { get; set; }

		public int MaximumGameTime { get; set; }

		public string DescriptiveVideoLink { get; set; }

		public LessonPlanTemplateGroup[] Grouping { get; set; }

		public LessonPlanTemplateCountry[] Countries { get; set; }

		public LessonPlanTemplateLanguage[] Languages { get; set; }

		public LessonPlanTemplateDifficulty[] DifficultyLevels { get; set; }

		public LessonPlanTemplateScenario[] ScenariosNames { get; set; }

		public LessonPlanTemplateParameter[] TemplateParameters { get; set; }
	}
}
