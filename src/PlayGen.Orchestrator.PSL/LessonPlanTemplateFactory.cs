using System.IO;
using Newtonsoft.Json;

using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.PSL.Model;

namespace PlayGen.Orchestrator.PSL
{
	public class LessonPlanTemplateFactory
	{
		private readonly LessonPlanTemplate _lessonPlanTemplate;

		public LessonPlanTemplateFactory(IRelativePathLocator relativePathLocator)
		{
            // Load in the lesson plan template for the gahe,
			//const string relativePath = @"Data/lessonPlanTemplate.json";
			const string relativePath = @"Data/lessonPlanTemplate.json";
			var json = File.ReadAllText(relativePathLocator.GetAbsoluteFromContentRelativePath(relativePath));
			_lessonPlanTemplate = JsonConvert.DeserializeObject<LessonPlanTemplate>(json);
		}

		public LessonPlanTemplate GetLessonPlanTemplate()
		{
			return _lessonPlanTemplate;
		}
	}
}
