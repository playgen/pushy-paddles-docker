using System;
using PlayGen.Orchestrator.PSL.Model;

namespace PlayGen.Orchestrator.PSL
{
	public interface ILessonInfoRepository
	{
		bool TryGetGameInstance(Guid gameInstanceId, out Match match);

		bool TryGetPlayerInfo(Guid playerId, out PlayerInfo playerInfo);
	}
}
