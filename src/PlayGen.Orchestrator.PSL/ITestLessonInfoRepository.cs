using System;
using System.Collections.Generic;
using PlayGen.Orchestrator.PSL.Model;

namespace PlayGen.Orchestrator.PSL
{
	public interface ITestLessonInfoRepository : ILessonInfoRepository
	{
		Lesson[] Lessons { get; }

		Match[] Matches { get; }

		IReadOnlyDictionary<Guid, PlayerInfo> Players { get; }

		object GetRandomPlayerToken();
	}
}
