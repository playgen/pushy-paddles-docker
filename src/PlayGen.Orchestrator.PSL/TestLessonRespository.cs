using System;
using System.Collections.Generic;
using System.Linq;

using PlayGen.Orchestrator.Contracts;
using PlayGen.Orchestrator.PSL.Model;

namespace PlayGen.Orchestrator.PSL
{
	public class TestLessonRespository : ITestLessonInfoRepository
	{
		#region name generator

		private static class Names
		{
			private static readonly Random _random = new Random();

			private static readonly string[] _names =
			{
				"Oliver",
				"Jack",
				"Harry",
				"George",
				"Jacob",
				"Charlie",
				"Noah",
				"William",
				"Thomas",
				"Oscar",
				"James",
				"Muhammad",
				"Henry",
				"Alfie",
				"Leo",
				"Joshua",
				"Freddie",
				"Ethan",
				"Archie",
				"Isaac",
				"Joseph",
				"Alexander",
				"Samuel",
				"Daniel",
				"Logan",
				"Edward",
				"Lucas",
				"Max",
				"Mohammed",
				"Benjamin",
				"Maso",
				"Harrison",
				"Theo",
				"Jake",
				"Sebastian",
				"Finley",
				"Arthur",
				"Adam",
				"Dylan",
				"Riley",
				"Zachary",
				"Teddy",
				"David",
				"Toby",
				"Theodore",
				"Elijah",
				"Matthew",
				"Jenson",
				"Jayden",
				"Harvey",
				"Reuben",
				"Harley",
				"Luca",
				"Michael",
				"Hugo",
				"Lewis",
				"Frankie",
				"Luke",
				"Stanley",
				"Tommy",
				"Jude",
				"Blake",
				"Louie",
				"Nathan",
				"Gabriel",
				"Charles",
				"Bobby",
				"Mohammad",
				"Ryan",
				"Tyler",
				"Elliott",
				"Albert",
				"Elliot",
				"Rory",
				"Alex",
				"Frederick",
				"Ollie",
				"Louis",
				"Dexter",
				"Jaxon",
				"Liam",
				"Jackson",
				"Callum",
				"Ronnie",
				"Leon",
				"Kai",
				"Aaron",
				"Roman",
				"Austin",
				"Ellis",
				"Jamie",
				"Reggie",
				"Seth",
				"Carter",
				"Felix",
				"Ibrahim",
				"Sonny",
				"Kian",
				"Caleb",
				"Connor",
				"Amelia",
				"Olivia",
				"Emily",
				"Isla",
				"Ava",
				"Ella",
				"Jessica",
				"Isabella",
				"Mia",
				"Poppy",
				"Sophie",
				"Sophia",
				"Lily",
				"Grace",
				"Evie",
				"Scarlett",
				"Ruby",
				"Chloe",
				"Isabelle",
				"Daisy",
				"Freya",
				"Phoebe",
				"Florence",
				"Alice",
				"Charlotte",
				"Sienna",
				"Matilda",
				"Evelyn",
				"Eva",
				"Millie",
				"Sofia",
				"Lucy",
				"Elsie",
				"Imogen",
				"Layla",
				"Rosie",
				"Maya",
				"Esme",
				"Elizabeth",
				"Lola",
				"Willow",
				"Ivy",
				"Erin",
				"Holly",
				"Emilia",
				"Molly",
				"Ellie",
				"Jasmine",
				"Eliza",
				"Lilly",
				"Abigail",
				"Georgia",
				"Maisie",
				"Eleanor",
				"Hannah",
				"Harriet",
				"Amber",
				"Bella",
				"Thea",
				"Annabelle",
				"Emma",
				"Amelie",
				"Harper",
				"Gracie",
				"Rose",
				"Summer",
				"Martha",
				"Violet",
				"Penelope",
				"Anna",
				"Nancy",
				"Zara",
				"Maria",
				"Darcie",
				"Maryam",
				"Megan",
				"Darcey",
				"Lottie",
				"Mila",
				"Heidi",
				"Lexi",
				"Lacey",
				"Francesca",
				"Robyn",
				"Bethany",
				"Julia",
				"Sara",
				"Aisha",
				"Darcy",
				"Zoe",
				"Clara",
				"Victoria",
				"Beatrice",
				"Hollie",
				"Arabella",
				"Sarah",
				"Maddison",
				"Leah",
				"Katie",
				"Aria",
			};

			public static string GetName()
			{
				return _names[_random.Next(_names.Length)];
			}
		}

		#endregion

		private readonly Dictionary<Guid, Lesson> _lessons = new Dictionary<Guid, Lesson>();

		private readonly Dictionary<Guid, Match> _gameInstances = new Dictionary<Guid, Match>();

		private readonly Dictionary<Guid, PlayerInfo> _players = new Dictionary<Guid, PlayerInfo>();

		#region random generation params

		private const int LessonsToGenerate = 1;

		private readonly LessonPlanTemplate _lessonPlanTemplate;

		#endregion

		#region constructors

		public TestLessonRespository(LessonPlanTemplateFactory lessonPlanTemplateFactory)
		{
			_lessonPlanTemplate = lessonPlanTemplateFactory.GetLessonPlanTemplate();

			GeneratePlayers();
			GenerateLessons();
		}

		#endregion

		#region dummy data generation

		private void GeneratePlayers()
		{
			var totalPlayers = _lessonPlanTemplate.Grouping.Sum(g => g.Teams.Sum(t => t.Count));

			for (var i = 0; i < totalPlayers; i++)
			{
				var playerId = Guid.NewGuid();
				while (_players.ContainsKey(playerId))
				{
					playerId = Guid.NewGuid();
				}
				var player = new PlayerInfo()
				{
					Id = playerId,
					Name = Names.GetName(),
				};
				_players.Add(playerId, player);
			}
		}

		private void GenerateLessons()
		{
			var random = new Random();

			for (var i = 0; i < LessonsToGenerate; i++)
			{
				var lessonId = Guid.NewGuid();
				while (_lessons.ContainsKey(lessonId))
				{
					lessonId = Guid.NewGuid();
				}
				var lesson = new Lesson()
				{
					Id = lessonId
				};

				var availablePlayers = _players.Values.ToList();
				var gameInstances = new List<Match>();

					
				foreach (var lpt in _lessonPlanTemplate.Grouping)
				{
					var gameInstanceId = Guid.NewGuid();
					while (_gameInstances.ContainsKey(gameInstanceId))
					{
						gameInstanceId = Guid.NewGuid();
					}
					var playerCount = lpt.Teams.Sum(t => t.Count);
					var gameInstance = new Match
					{
						Id = gameInstanceId,
						LessonId = lessonId,
						LessonPlan = new LessonPlan
						{
							difficulty = 2,
							language = "en",
							scenario = "default",
							maxPlayers = playerCount,
							maxTime = _lessonPlanTemplate.MaximumGameTime
						}
					};
					var players = new List<Guid>();
					for (var p = 0; p < playerCount; p++)
					{
						var index = random.Next(availablePlayers.Count);
						var player = availablePlayers[index];
						availablePlayers.RemoveAt(index);
						players.Add(player.Id);
					}
					gameInstance.PlayerIds = players.ToArray();
					gameInstances.Add(gameInstance);
					_gameInstances.Add(gameInstanceId, gameInstance);
				}
				lesson.Groups = gameInstances.Cast<LessonGroup>().ToArray();
				_lessons.Add(lessonId, lesson);
			}
		}

		#endregion

		#region Implementation of ILessonInfoRepository

		public Lesson[] Lessons => _lessons.Values.ToArray();

		public Match[] Matches => _gameInstances.Values.ToArray();

		public IReadOnlyDictionary<Guid, PlayerInfo> Players => _players;

		public object GetRandomPlayerToken()
		{
			return new
			{

			};
		}

		public bool TryGetGameInstance(Guid gameInstanceId, out Match match)
		{
			return _gameInstances.TryGetValue(gameInstanceId, out match);
		}

		public bool TryGetPlayerInfo(Guid playerId, out PlayerInfo playerInfo)
		{
			return _players.TryGetValue(playerId, out playerInfo);
		}


		#endregion
	}

}
