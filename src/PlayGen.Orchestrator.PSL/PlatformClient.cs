﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PlayGen.Orchestrator.PSL.Model;
using System.Linq;

using PlayGen.Orchestrator.Contracts;

namespace PlayGen.Orchestrator.PSL
{
	public class PlatformClient
	{
		private readonly LessonPlanTemplateFactory _lessonPlanTemplateFactory;

		public PlatformClient(LessonPlanTemplateFactory lessonPlanTemplateFactory)
		{
			_lessonPlanTemplateFactory = lessonPlanTemplateFactory;
		}

		public async Task<Match> TryGetMatch(string url, Guid matchId)
		{
			using (var client = new HttpClient())
			{
				client.BaseAddress = new Uri(url);
				var response = await client.GetStringAsync("/store/api/getMatchData/" + matchId);
				if (response != null)
				{
					var jsonDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(response);
					if (jsonDict.Keys.Count > 0)
					{
						var lessonPlanTemplate = _lessonPlanTemplateFactory.GetLessonPlanTemplate();
						var difficulty = jsonDict.ContainsKey("difficulty") ?
											int.Parse(jsonDict["difficulty"].ToString()) :
											jsonDict.ContainsKey("difficultyLevel") ?
											lessonPlanTemplate.DifficultyLevels.Any(d => d.Name == jsonDict["difficultyLevel"]) ?
											lessonPlanTemplate.DifficultyLevels.First(d => d.Name == jsonDict["difficultyLevel"]).Code :
											lessonPlanTemplate.DifficultyLevels.Min(d => d.Code) :
											lessonPlanTemplate.DifficultyLevels.Min(d => d.Code);
						var match = new Match
						{
							Id = matchId,
							Name = jsonDict.ContainsKey("name") ? jsonDict["name"].ToString() : lessonPlanTemplate.Name,
							LessonPlan = new LessonPlan
											{
												difficulty = difficulty,
												language = jsonDict.ContainsKey("language") ? jsonDict["language"].ToString() : "en",
												maxTime = jsonDict.ContainsKey("maxTime") ? int.Parse(jsonDict["maxTime"].ToString()) :
															jsonDict.ContainsKey("maximumGameTime") ? int.Parse(jsonDict["maximumGameTime"].ToString()) : 0,
												maxPlayers = jsonDict.ContainsKey("maxPlayers") ? int.Parse(jsonDict["maxPlayers"].ToString()) : 1,
												scenario = jsonDict.ContainsKey("scenario") ? jsonDict["scenario"].ToString() :
																jsonDict.ContainsKey("scenery") ? jsonDict["scenery"].ToString() : string.Empty
							},
							PlayerIds = JsonConvert.DeserializeObject<Guid[]>(jsonDict["students"].ToString())
						};

					    return match;
					}
					return null;
				}
				return null;
			}
		}
	}
}
