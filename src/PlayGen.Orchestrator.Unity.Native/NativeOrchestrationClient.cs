﻿using System;
using System.Text;
using BestHTTP;

using PlayGen.Orchestrator.Unity.Common;
using PlayGen.Unity.AsyncUtilities;

namespace PlayGen.Orchestrator.Unity.Native
{
	public class NativeOrchestrationClient : OrchestrationClientImplementationBase
	{
		protected override void OnInitializing()
		{
			WebServiceLocator = new WebServiceLocator(Config);
		}

		#region webservice abstraction

		public override void ServiceGET(string path, Action onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP GET {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError));
			request.Send();
		}

		public override void ServiceGET<T>(string path, Action<T> onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP GET {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError));
			request.Send();
		}

		public override void ServicePOST(string path, object payload, Action onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP POST {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError))
				{
					MethodType = HTTPMethods.Post,
					RawData = Encoding.UTF8.GetBytes(SerializeRequest(payload)),
				};
			request.SetHeader("Content-Type", "application/json");
			request.Send();
		}

		public override void ServicePOST<T>(string path, object payload, Action<T> onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP POST {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError)) {
				MethodType = HTTPMethods.Post,
				RawData = Encoding.UTF8.GetBytes(SerializeRequest(payload)),
			};
			request.SetHeader("Content-Type", "application/json");
			request.Send();
		}

		private void OnResponse(HTTPRequest request, HTTPResponse response, Action onSuccess, Action onError)
		{
			if (response.IsSuccess)
			{
				LogProxy.Debug($"OnResponse[{response.StatusCode}]");
				onSuccess();
			}
			else
			{
				LogProxy.Debug($"OnResponse[{response.StatusCode}] {response.DataAsText}");
				onError();
			}
		}

		private void OnResponse<T>(HTTPRequest request, HTTPResponse response, Action<T> onSuccess, Action onError)
		{
			try
			{
				if (response.IsSuccess)
				{
					LogProxy.Debug($"OnResponse[{response.StatusCode}] - Deserialize Response {response.DataAsText}");
					onSuccess(DeserializeResponse<T>(response.DataAsText));
				}
				else
				{
					LogProxy.Debug($"OnResponse[{response.StatusCode}] {response.DataAsText}");
					onError();
				}
			}
			catch (Exception ex)
			{
				onError();
			}
		}

		#endregion
	}
}
