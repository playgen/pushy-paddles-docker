using System;
using System.Text;
using BestHTTP;
using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Unity.Common;
using PlayGen.Orchestrator.Unity.Common.Model;
using PlayGen.Unity.AsyncUtilities;
using UnityEngine;

namespace PlayGen.Orchestrator.Unity.WebGL
{
	// ReSharper disable once InconsistentNaming
	public class WebGLOrchestrationClient : OrchestrationClientImplementationBase
	{
		// ReSharper disable once InconsistentNaming
		private const string WebGLProxy = "PlayGen.UnityProxy";

		protected override void OnInitializing()
		{
			LogProxy.Info("WebGLOrchestrationClient::OnInitializing");
			// signal the webgl javascript component that the webgl client proxy is initialized
			Application.ExternalCall($"{WebGLProxy}.OnInitializing", gameObject.name);
			
			// use the webgl service locator that calls javascript to obtain the application base url
			// this could be skipped if the orchestration server is hosted at a known absolute url
			WebServiceLocator = new WebGLWebServiceLocator(Config);
			Initialized += ClientInitialized;
		}

		public void ClientInitialized()
		{
			// signal javascript - we're in a state to receive callback
			Application.ExternalCall($"{WebGLProxy}.Initialized");
		}

		#region external calls

		/// <summary>
		/// Window DOM event - called from JS
		/// </summary>
		public void OnWindowBeforeUnload()
		{
			OnBeforeShutdown();
		}

		/// <summary>
		///	provide json serialized session identifiers - from JS
		/// </summary>
		/// <param name="identifierString"></param>
		public void OnPlayerIdentifier(string identifierString)
		{
			try
			{
				var sessionIdentifier = JsonUtility.FromJson<SessionIdentifier>(identifierString);
				OnPlayerIdentified(sessionIdentifier);
			}
			catch (Exception ex)
			{
				LogProxy.Exception(ex);
			}
		}

		#endregion

		#region webservice abstraction

		public override void ServiceGET(string path, Action onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP GET {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError));
			request.Send();
		}

		public override void ServiceGET<T>(string path, Action<T> onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP GET {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError));
			request.Send();
		}

		public override void ServicePOST(string path, object payload, Action onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP POST {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError)) {
				MethodType = HTTPMethods.Post,
				RawData = Encoding.UTF8.GetBytes(SerializeRequest(payload)),
			};
			request.SetHeader("Content-Type", "application/json");
			request.Send();
		}

		public override void ServicePOST<T>(string path, object payload, Action<T> onSuccess, Action onError)
		{
			var url = GetServiceUrl(path);
			LogProxy.Debug($"HTTP POST {url}");
			var request = new HTTPRequest(new Uri(url), (req, res) => OnResponse(req, res, onSuccess, onError)) {
				MethodType = HTTPMethods.Post,
				RawData = Encoding.UTF8.GetBytes(SerializeRequest(payload)),
			};
			request.SetHeader("Content-Type", "application/json");
			request.Send();
		}

		private void OnResponse(HTTPRequest request, HTTPResponse response, Action onSuccess, Action onError)
		{
			if (response.IsSuccess)
			{
				LogProxy.Debug($"OnResponse[{response.StatusCode}]");
				onSuccess();
			}
			else
			{
				LogProxy.Debug($"OnResponse[{response.StatusCode}] {response.DataAsText}");
				onError();
			}
		}

		private void OnResponse<T>(HTTPRequest request, HTTPResponse response, Action<T> onSuccess, Action onError)
		{
			try
			{
				if (response.IsSuccess)
				{
					LogProxy.Debug($"OnResponse[{response.StatusCode}] - Deserialize Response {response.DataAsText}");
					onSuccess(DeserializeResponse<T>(response.DataAsText));
				}
				else
				{
					LogProxy.Debug($"OnResponse[{response.StatusCode}] {response.DataAsText}");
					onError();
				}
			}
			catch (Exception ex)
			{
				onError();
			}
		}

		#endregion


		/// <summary>
		/// additional handler to set the web socket URL incerption value in UNETWebSockets.jspre
		/// </summary>
		/// <param name="endpoint"></param>
		public override void OnEndpointLocated(Endpoint endpoint)
		{
			Application.ExternalCall($"{WebGLProxy}.SetWebSocketURL", endpoint.port);
		}

		#region Overrides of OrchestrationClientImplementationBase

		public override void OnEndpointLocationError(EndpointStatus status)
		{
			Application.ExternalCall($"{WebGLProxy}.OnEndpointLocationError");
		}

		#endregion
	}
}
