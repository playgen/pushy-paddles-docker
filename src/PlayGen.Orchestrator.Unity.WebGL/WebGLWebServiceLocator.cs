using System.Runtime.InteropServices;

using PlayGen.Orchestrator.Unity.Common;
using PlayGen.Orchestrator.Unity.Common.Config;
using PlayGen.Unity.AsyncUtilities;

namespace PlayGen.Orchestrator.Unity.WebGL
{
	// ReSharper disable once InconsistentNaming
	public class WebGLWebServiceLocator : WebServiceLocator
	{
		public WebGLWebServiceLocator(OrchestrationClientConfig config) 
			: base(config)
		{
		}

		#region Overrides of WebServiceLocator

		public override string GetWebServiceUrlBase()
		{
			LogProxy.Debug(nameof(WebGLWebServiceLocator) + nameof(GetWebServiceUrlBase));
			return Url ?? (Url = GetApplicationBase() + Config.ServiceUrlSuffix);
		}

		#endregion

		[DllImport("__Internal")]
		public static extern string GetApplicationBase();
	}
}
