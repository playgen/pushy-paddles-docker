﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Core;
using PlayGen.Orchestrator.PSL.Common;
using PlayGen.Orchestrator.PSL.Common.LRS;
using PlayGen.Orchestrator.PSL.Model;
using PlayGen.Orchestrator.PSL.Contracts.LRS;

namespace PlayGen.Orchestrator.PSL.Core
{
	public class PSLGameManager : GameManager<PSLGameInstance>
	{
		private readonly PlatformClient _platformClient;

		public PSLGameManager(PlatformClient platformClient, GameProcessManager processManager) : base(processManager)
		{
			_platformClient = platformClient;
		}

		public async Task<bool> ValidateToken(string url, string token)
		{
			using (var client = new HttpClient())
			{
				client.BaseAddress = new Uri(url);
				var response = await client.PostAsync("/store/api/tokenValidation/" + token + "/", new FormUrlEncodedContent(new Dictionary<string, string>()));
				if (response != null)
				{
					var result = await response.Content.ReadAsStringAsync();
					return result != null && result.ToLower().Contains("true");
				}
				return false;
			}
		}

		/// <summary>
		/// This will return the game endpoint for a requested game instance, which will be started on demand if required
		/// </summary>
		/// <param name="gameIdentifier">game identifier for this isntance</param>
		/// <param name="playerIdentifier">player identifier, not currently needed</param>
		/// <param name="pslStoreUrl"></param>
		/// <returns></returns>
		public Task<Endpoint> JoinCreatePSLGameInstance(GameIdentifier gameIdentifier, PlayerIdentifier playerIdentifier, string pslStoreUrl, out EndpointStatus status)
		{
			lock (_lock)
			{
				// if the game is not already in the cache
				if (_games.TryGetValue(gameIdentifier.Id, out var gameInstance) == false)
				{
					var match = _platformClient.TryGetMatch(pslStoreUrl, gameIdentifier.Id);
                    // wait for match async
				    match.Wait();

                    if (match.Result != null)
					{
						gameInstance = match.Result.ToGameInstance();
						gameInstance.StoreUrl = pslStoreUrl;
						SetLrsUrl(gameInstance, pslStoreUrl);
						// try and start the game
						_processManager.StartServer(gameInstance);
						// cache it
						_games.Add(gameInstance.GameIdentifier.Id, gameInstance);
					}
					else
					{
						status = EndpointStatus.MatchFailure;
						return Task.FromResult<Endpoint>(null);
					}
				}
				else if (gameInstance.Players.Any(p => p.Connected && p.Id == playerIdentifier.Id))
				{
					status = EndpointStatus.MatchAlreadyJoined;
					return Task.FromResult<Endpoint>(null);
				}
				else if (gameInstance.Players.All(p => p.Id != playerIdentifier.Id) && gameInstance.Players.Length >= gameInstance.LessonPlan.maxPlayers)
				{
					status = EndpointStatus.MatchFull;
					return Task.FromResult<Endpoint>(null);
				}
				status = EndpointStatus.Success;
				// return the task completion source (promise) which will immediately yield a vlue if the game is running or will block until the game server calls RegisterGame
				return gameInstance.EndpointCompletionSource.Task;
			}
		}

	    public Task<Endpoint> DummyJoinCreatePSLGameInstance(GameIdentifier gameIdentifier, PlayerIdentifier playerIdentifier, string pslStoreUrl, out EndpointStatus status)
	    {
	        // if the game is not already in the cache
	            if (_games.TryGetValue(gameIdentifier.Id, out var gameInstance) == false)
	            {
	                var match = _platformClient.TryGetMatch(pslStoreUrl, gameIdentifier.Id);
	                match.Wait();
	                if (match.Result != null)
	                {
	                    gameInstance = match.Result.ToGameInstance();
	                    gameInstance.StoreUrl = pslStoreUrl;
	                    SetLrsUrl(gameInstance, pslStoreUrl);

                        //_processManager.StartServer(gameInstance);
                        // cache it
                        _games.Add(gameInstance.GameIdentifier.Id, gameInstance);
	                }
	                else
	                {
	                    status = EndpointStatus.MatchFailure;
	                    return Task.FromResult<Endpoint>(null);
	                }
	            }
	            else if (gameInstance.Players.Any(p => p.Connected && p.Id == playerIdentifier.Id))
	            {
	                status = EndpointStatus.MatchAlreadyJoined;
	                return Task.FromResult<Endpoint>(null);
	            }
	            else if (gameInstance.Players.All(p => p.Id != playerIdentifier.Id) && gameInstance.Players.Length >= gameInstance.LessonPlan.maxPlayers)
	            {
	                status = EndpointStatus.MatchFull;
	                return Task.FromResult<Endpoint>(null);
	            }
	            status = EndpointStatus.Success;
	            // return the task completion source (promise) which will immediately yield a vlue if the game is running or will block until the game server calls RegisterGame
	            return gameInstance.EndpointCompletionSource.Task;

	    }


        private async void SetLrsUrl(PSLGameInstance gameInstance, string url)
		{
			using (var client = new HttpClient())
			{
				client.BaseAddress = new Uri(url);
				var response = await client.GetStringAsync("/store/api/getLRS/" + gameInstance.GameIdentifier.Id);
				if (response != null)
				{
					var jsonDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);
					if (jsonDict != null && jsonDict.Keys.Count > 0 && jsonDict.ContainsKey("URL") && jsonDict.ContainsKey("uid") && jsonDict.ContainsKey("pass"))
					{
						gameInstance.LrsUrl = "http://" + jsonDict["URL"] + "/data/xAPI/";
						gameInstance.LrsUsername = jsonDict["uid"];
						gameInstance.LrsPassword = jsonDict["pass"];
					}
				}
			}
		}

		public async void PostToLrs(LRSDataRequest data)
		{
			if (_games.TryGetValue(new Guid(data.MatchId), out var gameInstance))
			{
				var formattedData = GetFormattedData(data, gameInstance);
				if (!string.IsNullOrEmpty(gameInstance.LrsUrl) && !string.IsNullOrEmpty(gameInstance.LrsUsername) && !string.IsNullOrEmpty(gameInstance.LrsPassword))
				{
					using (var client = new HttpClient())
					{
						client.BaseAddress = new Uri(gameInstance.LrsUrl);
						client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
							Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", gameInstance.LrsUsername, gameInstance.LrsPassword))));
						client.DefaultRequestHeaders.Add("Content-Type", "application/json");
						client.DefaultRequestHeaders.Add("X-Experience-API-Version", "1.0.2");
						await client.PutAsync("statements?statementId=" + new Guid(), new StringContent(formattedData));
					}
				}
			}
		}

		private string GetFormattedData(LRSDataRequest data, PSLGameInstance gameInstance)
		{
			var formattedData = new LRSContent();
			formattedData.TimeStamp = DateTime.UtcNow.ToString("s", System.Globalization.CultureInfo.InvariantCulture) + "Z";
			formattedData.Version = "1.0.0";
			formattedData.Actor = new LRSActor
			{
				ObjectType = "Agent",
				Account = new LRSAccount { HomePage = "http://prosociallearn.eu/pids/", Name = data.PlayerId }
			};
			formattedData.Verb = new LRSVerb { Id = "http://prosociallearn.eu/plsxapi/verbs/" + data.Verb };
			formattedData.Result = new LRSResult
			{
				Score = new LRSScore { Raw = data.Value < data.Verb.GetMinRange() ? data.Verb.GetMinRange() : data.Value }
			};
			formattedData.Context = new LRSContext { ContextActivities = new LRSContextActivities { Parent = new List<LRSObject>() } };
			formattedData.Object = new LRSObject { Id = "http://prosociallearn.eu/aiids/" + data.MatchId, ObjectType = "Activity" };

			formattedData.Context.ContextActivities.Parent.Add(new LRSObject
			{
				Id = "http://prosociallearn.eu/aiids/" + data.MatchId,
				ObjectType = "Activity"
			});

			if (data.Verb.GetUsesSocial())
			{
				var memberList = new List<LRSActor>();
				foreach (var player in gameInstance.Players)
				{
					if (player.Id.ToString() != data.PlayerId)
					{
						memberList.Add(new LRSActor
						{
							ObjectType = "Agent",
							Account = new LRSAccount
							{
								HomePage = "http://prosociallearn.eu/pids/",
								Name = player.Id.ToString()
							}
						});
					}
				}
				formattedData.Context.Team = new LRSTeam { Member = memberList };
			}

			return JsonConvert.SerializeObject(formattedData);
		}

		protected override async void ConnectPlayer(Guid matchId, Guid playerId)
		{
			if (_games.TryGetValue(matchId, out var gameInstance))
			{
				using (var client = new HttpClient())
				{
					client.BaseAddress = new Uri(gameInstance.StoreUrl);
					await client.PostAsync("/store/api/connectPlayer?matchId=" + matchId + "&playerId=" + playerId, new FormUrlEncodedContent(new Dictionary<string, string>()));
				}
			}
		}
	}
}
