﻿using System;
using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Unity.Common.Config;
using PlayGen.Orchestrator.Unity.Common.Model;

namespace PlayGen.Orchestrator.Unity.Common
{
	public interface IOrchestrationClientImplementation
	{
		void Initialize(OrchestrationClientConfig config);
		event Action Initialized;

		event Action BeforeShutdown;

		event Action<SessionIdentifier> PlayerIdentified;

		WebServiceLocator WebServiceLocator { get; }

		// ReSharper disable InconsistentNaming
		void ServiceGET(string path, Action onSuccess, Action onError);

		void ServiceGET<T>(string path, Action<T> onSuccess, Action onError);

		void ServicePOST(string path, object payload, Action onSuccess, Action onError);

		void ServicePOST<T>(string path, object payload, Action<T> onSuccess, Action onError);
		// ReSharper restore InconsistentNaming

		void OnEndpointLocated(Endpoint endpoint);
		void OnEndpointLocationError(EndpointStatus status);

		void OnPlayerIdentified(SessionIdentifier identifier);
		void OnPlayerIdentificationError();

	}
}
