using UnityEngine;

namespace PlayGen.Orchestrator.Unity.Common.Config
{
	public class OrchestrationClientConfig
	{
		public LogType LogLevel;

		public string ServiceUrl;

		public string ServiceUrlSuffix;
	}
}
