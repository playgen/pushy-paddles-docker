using System;

using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Unity.Common.Config;
using PlayGen.Orchestrator.Unity.Common.Model;
using PlayGen.Unity.AsyncUtilities;
using UnityEngine;

namespace PlayGen.Orchestrator.Unity.Common
{
	public abstract class OrchestrationClientImplementationBase : MonoBehaviour, IOrchestrationClientImplementation
	{
		public event Action Initialized;

		public event Action BeforeShutdown;
		public event Action<SessionIdentifier> PlayerIdentified;
		public event Action<Endpoint> EndpointLocated;

		protected OrchestrationClientConfig Config { get; private set; }

		public WebServiceLocator WebServiceLocator { get; set; }

		protected virtual void OnBeforeShutdown()
		{
			BeforeShutdown?.Invoke();
		}

		public void Initialize(OrchestrationClientConfig config)
		{
			Config = config;
			// virtual call to the subclasses
			OnInitializing();
			Initialized?.Invoke();
		}

		public virtual void OnPlayerIdentified(SessionIdentifier sessionIdentifier)
		{
			// use an event so the session details can be pushed in async from somehwere external (webgl)
			PlayerIdentified?.Invoke(sessionIdentifier);
		}

		public virtual void OnPlayerIdentificationError()
		{

		}

		public virtual void OnEndpointLocated(Endpoint endpoint)
		{

		}

		public virtual void OnEndpointLocationError(EndpointStatus status)
		{

		}


		protected abstract void OnInitializing();

		#region web service abstraction

		protected string GetServiceUrl(string path)
		{
			return WebServiceLocator.GetWebServiceUrlBase() + path;
		}

		public abstract void ServiceGET(string path, Action onSuccess, Action onError);

		public abstract void ServiceGET<T>(string path, Action<T> onSuccess, Action onError);

		public abstract void ServicePOST(string path, object payload, Action onSuccess, Action onError);

		public abstract void ServicePOST<T>(string path, object payload, Action<T> onSuccess, Action onError);

		protected T DeserializeResponse<T>(string data)
		{
			LogProxy.Debug($"DeserializeResponse: {data}");
			return JsonUtility.FromJson<T>(data);
		}

		protected string SerializeRequest<T>(T payload)
		{
			var json = JsonUtility.ToJson(payload);
			LogProxy.Debug($"SerializeRequest: {json}");
			return json;
		}

		#endregion
	}
}
