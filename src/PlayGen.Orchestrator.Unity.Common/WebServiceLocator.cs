using PlayGen.Orchestrator.Unity.Common.Config;
using PlayGen.Unity.AsyncUtilities;

namespace PlayGen.Orchestrator.Unity.Common
{
	public class WebServiceLocator
	{
		protected string Url;

		protected OrchestrationClientConfig Config;

		public WebServiceLocator(OrchestrationClientConfig config)
		{
			Config = config;
		}

		public virtual string GetWebServiceUrlBase()
		{
			LogProxy.Debug(nameof(WebServiceLocator) + nameof(GetWebServiceUrlBase));
			return Url ?? (Url = Config.ServiceUrl + Config.ServiceUrlSuffix);
		}
	}
}
