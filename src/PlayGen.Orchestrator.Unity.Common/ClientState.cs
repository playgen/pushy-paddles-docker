namespace PlayGen.Orchestrator.Unity.Common
{
	public enum ClientState
	{
		NotInitialized = 0,

		Initializing,	// loading config
		Initialized,

	}
}
