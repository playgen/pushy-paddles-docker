﻿using PlayGen.Orchestrator.Common;

namespace PlayGen.Orchestrator.PSL.Common
{
	public class PSLGameInstance : GameInstance
	{
		public PSLGameInstance(GameIdentifier gameId) : base(gameId)
		{
		}

		public string StoreUrl { get; set; }

		public string LrsUrl { get; set; }

		public string LrsUsername { get; set; }

		public string LrsPassword { get; set; }
	}
}
