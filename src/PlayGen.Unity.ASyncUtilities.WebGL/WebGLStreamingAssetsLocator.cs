﻿using System.Runtime.InteropServices;

using PlayGen.Unity.AsyncUtilities;

namespace PlayGen.Unity.ASyncUtilities.WebGL
{
	public class WebGLStreamingAssetsLocator : AsyncConfigLoader.StreamingAssetsLocator
	{
		#region Overrides of StreamingAssetsLocator

		public override string StreamingassetsPath => GetStreamingAssetsPath();


		#endregion

		[DllImport("__Internal")]
		public static extern string GetStreamingAssetsPath();
	}
}
