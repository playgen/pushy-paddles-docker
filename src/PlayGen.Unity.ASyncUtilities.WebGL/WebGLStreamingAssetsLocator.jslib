﻿var WebGLStreamingAssetsLocatorPlugin = { 
	GetStreamingAssetsPath: function () {
		var debug = true;
		debug && console.debug("GetStreamingAssetsPath");

		var path = window.PlayGen.UnityProxy.GetApplicationBase() + window.PlayGen.UnityProxy.Config.StreamingAssetsPath;

		var buffer = _malloc(lengthBytesUTF8(path) + 1);
			
		debug && console.debug("HttpRequest::writeStringToMemory");
		writeStringToMemory(path, buffer);
		return buffer;
	}
};

mergeInto(LibraryManager.library, WebGLStreamingAssetsLocatorPlugin);
