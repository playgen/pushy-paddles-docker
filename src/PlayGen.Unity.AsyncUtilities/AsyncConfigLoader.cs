﻿using System;
using System.Collections;
using UnityEngine;

namespace PlayGen.Unity.AsyncUtilities
{
	public class AsyncConfigLoader : MonoBehaviour
	{
		public class StreamingAssetsLocator
		{
			public virtual string StreamingassetsPath => Application.streamingAssetsPath;
		}

		// ReSharper disable once InconsistentNaming
		private const string WebGLProxy = "PlayGen.UnityProxy";

		private static AsyncConfigLoader _instance;

		public static StreamingAssetsLocator Locator { get; set; } = new StreamingAssetsLocator();

		public void Awake()
		{
			if (_instance != null)
			{
				Destroy(this);
			}
			else
			{
				_instance = this;
				DontDestroyOnLoad(this);
			}
		}

		/// <summary>
		/// Asynchronously read a simple (JsonUtility serializable) object from StreamingAssets
		/// </summary>
		public static void ReadConfig<T>(string path, Action<T> onComplete, Action<Exception> onError)
		{
			try
			{
				var job = new Func<IEnumerator>(() => JobWorker(path, onComplete, onError));
				_instance.StartCoroutine(job());
			}
			catch (Exception ex)
			{
				onError(ex);
			}
		}

		private static IEnumerator JobWorker<T>(string path, Action<T> onComplete, Action<Exception> onError)
		{
			path = $"{Locator.StreamingassetsPath}/{path}";
			if (Application.platform == RuntimePlatform.WindowsEditor ||
				Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WSAPlayerX86 ||
				Application.platform == RuntimePlatform.WSAPlayerX64 || Application.platform == RuntimePlatform.LinuxPlayer)
			{
				path = "file:///" + path;
			}
			LogProxy.Info($"AsyncConfigLoader::ReadConfig({path})");
			var www = new WWW(path);
			yield return www;

			try
			{
				var config = JsonUtility.FromJson<T>(www.text);
				onComplete(config);
			}
			catch (Exception ex)
			{
				onError(ex);
			}
		}

	}
}
