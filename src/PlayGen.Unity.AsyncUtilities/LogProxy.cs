﻿using System;

using UnityEngine;

namespace PlayGen.Unity.AsyncUtilities
{
	public static class LogProxy
	{
		public static bool LogDebug { get; set; } = true;

		public static LogType LogLevel
		{
			get => Logger.filterLogType;
			set => Logger.filterLogType = value;
		}

		public static ILogger Logger { get; set; } = UnityEngine.Debug.logger;

		public static void Exception(Exception exception)
		{
			Logger.Log(LogType.Error, $"[ERROR] {exception}");
		}
		public static void Exception(string message, Exception exception)
		{
			Logger.Log(LogType.Error, $"[ERROR] {message}: {exception}");
		}

		public static void Error(string message)
		{
			Logger.Log(LogType.Error, $"[ERROR] {message}");
		}

		public static void Warning(string message)
		{
			Logger.Log(LogType.Warning, $"[WARNING] {message}");
		}

		public static void Info(string message)
		{
			Logger.Log(LogType.Log, $"[INFO] {message}");
		}

		public static void Debug(string message)
		{
			if (LogDebug)
			{
				Logger.Log(LogType.Log, $"[DEBUG] {message}");
			}
		}

		public static void Assert(string message)
		{
			Logger.Log(LogType.Assert, message);
		}

	}
}
