﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PlayGen.Unity.AsyncUtilities
{
	public class IntervalExecutor : MonoBehaviour
	{
		private class IntervalTask
		{
			private readonly object _lock = new object();

			private readonly int _taskId;

			private readonly string _name;

			private readonly Action _action;

			private readonly float _interval;

			private float _progress;

			private int _repeat;

			public IntervalTask(int taskId, string name, Action action, float interval, int repeat = -1)
			{
				_taskId = taskId;
				_name = name;
				_action = action;
				_interval = interval;
				_repeat = repeat;
			}

			public void Update()
			{
				//LogProxy.Info($"IntervalTask [{_taskId}]{_name}: Update");
				lock (_lock)
				{
					_progress += Time.deltaTime;
					if (_progress >= _interval)
					{
						_progress = 0;
						if (_repeat < 0 || _repeat > 0)
						{
							_repeat = _repeat - 1;
							try
							{
								LogProxy.Debug($"Executing interval task [{_taskId}]{_name}: count {_repeat}");
								_action();
							}
							catch (Exception ex)
							{
								LogProxy.Error($"Error executing periodic task [{_taskId}] {_name}");
								LogProxy.Exception(ex);
							}
						}
						else if (_repeat == 0)
						{
							LogProxy.Info($"Terminating periodic task [{_taskId}] {_name}, repetion count completed.");
							CancelTask(_taskId);
						}
					}
				}
			}
		}

		private static IntervalExecutor _instance;

		private readonly Dictionary<int, IntervalTask> _tasks = new Dictionary<int, IntervalTask>();

		private int _taskId;

		public void Awake()
		{
			if (_instance != null)
			{
				Destroy(this);
			}
			else
			{
				_instance = this;
				DontDestroyOnLoad(this);
			}
		}

		public static int EnqueueIntervalTask(Action action, string name, float interval, int repeat = -1)
		{
			lock (_instance._tasks)
			{
				var taskId = _instance._taskId++;
				LogProxy.Info($"Enqueuing Interval task [{taskId}]{name} Interval: {interval}, Repeat:{repeat}");
				_instance._tasks.Add(taskId, new IntervalTask(taskId, name, action, interval, repeat));
				return taskId;
			}
		}

		public static void CancelTask(int taskId)
		{
			lock (_instance._tasks)
			{
				_instance._tasks.Remove(taskId);
			}
		}

		void Update()
		{
			foreach (var task in _tasks.Values.ToArray())
			{
				task.Update();
			}
		}

	}
}
