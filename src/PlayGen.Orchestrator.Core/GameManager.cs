﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PlayGen.Orchestrator.Common;
using System.Linq;
using PlayGen.Orchestrator.Contracts;

namespace PlayGen.Orchestrator.Core
{
	public class GameManager<T> where T : GameInstance
	{
		protected readonly GameProcessManager _processManager;

		protected readonly Dictionary<Guid, T> _games;

		public Dictionary<Guid, T> Games => _games;

		protected readonly object _lock = new object();

		public GameManager(GameProcessManager processManager)
		{
			_processManager = processManager;
			_processManager.ServerTerminated += _processManager_ServerTerminated;
			_games = new Dictionary<Guid, T>();
		}

		private void _processManager_ServerTerminated(Guid id)
		{
			_games.Remove(id);
		}

		/// <summary>
		/// This will return the game endpoint for a requested game instance, which will be started on demand if required
		/// </summary>
		/// <param name="gameIdentifier">game identifier for this isntance</param>
		/// <param name="playerIdentifier">player identifier, not currently needed</param>
		/// <returns></returns>
		public Task<Endpoint> JoinCreateGameInstance(GameIdentifier gameIdentifier, PlayerIdentifier playerIdentifier)
		{
			lock (_lock)
			{
				return null;
			}
		}

		/// <summary>
		/// called by the game server after the OrchestratedServer has initialized
		/// signals that the instance is ready to accept plaeyr connections
		/// </summary>
		/// <param name="gameId">instance identifier</param>
		/// <param name="state">current state of the game server (WaitingForStart or WaitingForPlayers)</param>
		public void RegisterGame(Guid gameId, GameState state)
		{
			lock (_lock)
			{
				if (_games.TryGetValue(gameId, out var gameInstance))
				{
					gameInstance.State = state;
					gameInstance.EndpointCompletionSource.SetResult(gameInstance.Endpoint);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="gameId"></param>
		/// <returns></returns>
		public LessonPlan GetLessonPlan(Guid gameId)
		{
			lock (_lock)
			{
				if (_games.TryGetValue(gameId, out var gameInstance))
				{
					return gameInstance.LessonPlan;
				}
			}
			throw new InvalidOperationException($"Could not get game instance for id: {gameId}");
		}

		public GameState GetState(Guid gameId)
		{
			lock (_lock)
			{
				if (_games.TryGetValue(gameId, out var gameInstance))
				{
					return gameInstance.State;
				}
			}
			return GameState.NotInitialized;
		}

		public int UpdatePlayers(Guid gameId, List<string> players)
		{
			lock (_lock)
			{
				if (_games.TryGetValue(gameId, out var gameInstance))
				{
					foreach (var player in players)
					{
						if (gameInstance.Players.All(p => p.Id.ToString() != player))
						{
							AddPlayer(gameInstance, player);
						}
					}
					foreach (var player in gameInstance.Players)
					{
						player.SetConnectionState(players.Contains(player.Id.ToString()));
					}
					return gameInstance.Players.Count(p => p.Connected);
				}
			}
			return 0;
		}

		protected virtual void AddPlayer(T gameInstance, string playerId)
		{
			var playerArray = gameInstance.Players;
			var newPlayer = new PlayerIdentifier(playerId);
			var playerList = playerArray.ToList();
			playerList.Add(newPlayer);
			playerArray = playerList.ToArray();
			gameInstance.Players = playerArray;
			ConnectPlayer(gameInstance.GameIdentifier.Id, newPlayer.Id);
		}

		protected virtual void ConnectPlayer(Guid matchId, Guid playerId)
		{
		}

		public T GetGame(Guid gameId)
		{
			lock (_lock)
			{
				if (_games.TryGetValue(gameId, out var gameInstance))
				{
					return gameInstance;
				}
			}
			return null;
		}

		public GameState UpdateState(Guid gameId, GameState reportedState)
		{
			lock (_lock)
			{
				if (_games.TryGetValue(gameId, out var gameInstance))
				{
					if (gameInstance.StateChangePending)
					{
						if (reportedState == gameInstance.RequestedState)
						{
							gameInstance.StateChangePending = false;
						}
						gameInstance.State = reportedState;
						return gameInstance.RequestedState;
					}
					gameInstance.State = reportedState;
					return gameInstance.State;
				}
			}
			throw new InvalidOperationException($"Could not get game instance for id: {gameId}");
		}
	}
}
