using System;

namespace PlayGen.Orchestrator.Core.Configuration
{
	public class EnvironmentVariableConfigLoader
	{
		private const string GameServerExecutableEnvVar = "GAME_SERVER_EXECUTABLE";
		private const string GameServerListenAddressEnvVar = "GAME_SERVER_LISTEN";
		private const string GameServerBasePortEnvVar = "GAME_SERVER_BASE_PORT";
		private const string GameServerMaxInstancesEnvVar = "GAME_SERVER_MAX_INSTANCES";

		public static Config GetConfiguration()
		{

			var gameServerExecutable = Environment.GetEnvironmentVariable(GameServerExecutableEnvVar);

			if (string.IsNullOrEmpty(gameServerExecutable))
			{
				throw new Exception($"Missing configuration variable: {GameServerExecutableEnvVar}");
			}


			var gameServerListenAddress = Environment.GetEnvironmentVariable(GameServerListenAddressEnvVar) ?? "0.0.0.0";

			var gameServerBasePort = Environment.GetEnvironmentVariable(GameServerBasePortEnvVar) ?? "9000";

			var gameServerMaxInstances = Environment.GetEnvironmentVariable(GameServerMaxInstancesEnvVar) ?? "30";

			return new Config
			{
				GameServerListenAddress = gameServerListenAddress,
				GameServerExecutablePath = gameServerExecutable,
				GameServerBasePort = Int32.Parse(gameServerBasePort),
				GameServerMaxInstances = Int32.Parse(gameServerMaxInstances),
			};
		}
	}
}
