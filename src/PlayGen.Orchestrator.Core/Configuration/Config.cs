namespace PlayGen.Orchestrator.Core.Configuration
{
	public class Config
	{
		public string GameServerExecutablePath { get; set; }

		public string GameServerListenAddress { get; set; }

		public int GameServerBasePort { get; set; }

		public int GameServerMaxInstances { get; set; }


	}
}
