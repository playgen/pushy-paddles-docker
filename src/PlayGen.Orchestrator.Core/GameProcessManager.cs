using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Options;
using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Core.Configuration;

namespace PlayGen.Orchestrator.Core
{
	public class GameProcessManager
	{
		#region process cache class 

		public class ProcessWrapper
		{
			public Process Process { get; set; }

			public GameInstance GameInstance { get; set; }
		}

		#endregion

		public event Action<Guid> ServerTerminated;
 
		private readonly Dictionary<int, ProcessWrapper> _serverProcessesByPid;
		private readonly Dictionary<Guid, ProcessWrapper> _serverProcessesByGameId;

		private readonly Queue<int> _availablePorts;

		private readonly Config _config;
		private readonly OrchestratorSettings _orchestratorSettings;

		public GameProcessManager(Config config, IOptions<OrchestratorSettings> orchestratorSettings)
		{
			_config = config;
			_orchestratorSettings = orchestratorSettings.Value;

			_serverProcessesByPid = new Dictionary<int, ProcessWrapper>();
			_serverProcessesByGameId = new Dictionary<Guid, ProcessWrapper>();
			_availablePorts = new Queue<int>();

			GenerateEndpointPool();
		}

		private void GenerateEndpointPool()
		{
			foreach (var port in Enumerable.Range(_config.GameServerBasePort, _config.GameServerMaxInstances))
			{
				_availablePorts.Enqueue(port);
			}
		}

		private Endpoint GetEndpoint()
		{
			return new Endpoint()
			{
				host = _config.GameServerListenAddress,
				port = _availablePorts.Dequeue(),
				protocol = EndpointProtocol.UNETWebSocketSecure,
			};
		}

		public void StartServer(GameInstance gameInstance)
		{
			try
			{
				gameInstance.State = GameState.Initializing;
				gameInstance.Endpoint = GetEndpoint();

				var initialArgs = $"-logFile {Path.Combine(_orchestratorSettings.GameServerLogPath, $"{gameInstance.GameIdentifier.Id}.log")}";
				var serverArgs = GameServerArguments.GenerateArguments(gameInstance.Endpoint.host, gameInstance.Endpoint.port, gameInstance.LessonPlan.maxPlayers, StartMode.PlayersAvailable, gameInstance.GameIdentifier.Id);
				var args = $"{initialArgs} {serverArgs}";
				
				var process = new Process()
				{
					EnableRaisingEvents = true,
					StartInfo = new ProcessStartInfo(_config.GameServerExecutablePath, args),
				};

				process.Exited += GameServer_Exited;

				var pw = new ProcessWrapper()
				{
					GameInstance = gameInstance,
					Process = process,
				};

				process.Start();

				_serverProcessesByPid.Add(process.Id, pw);
				_serverProcessesByGameId.Add(gameInstance.GameIdentifier.Id, pw);
			}
			catch (Exception)
			{
				gameInstance.State = GameState.Error;
				throw;
			}
		}

		private void GameServer_Exited(object sender, EventArgs e)
		{
			var process = sender as Process;
			if (process == null)
			{
				throw new Exception("Exited event raised by non process!");
			}
			if (_serverProcessesByPid.TryGetValue(process.Id, out var processWrapper))
			{
				_availablePorts.Enqueue(processWrapper.GameInstance.Endpoint.port);
				_serverProcessesByGameId.Remove(processWrapper.GameInstance.GameIdentifier.Id);
				OnServerTerminated(processWrapper.GameInstance.GameIdentifier.Id);
			}
		}

		public void StopServer(Guid instanceId)
		{
			if (_serverProcessesByGameId.TryGetValue(instanceId, out var processWrapper) == false)
			{
				throw new InvalidOperationException($"GameInstance not found. Id: {instanceId}");
			}
			processWrapper.Process.Kill();
		}

		protected virtual void OnServerTerminated(Guid obj)
		{
			ServerTerminated?.Invoke(obj);
		}
	}
}
