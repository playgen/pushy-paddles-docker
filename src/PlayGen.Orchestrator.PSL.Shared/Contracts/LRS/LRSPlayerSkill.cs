﻿using PlayGen.Orchestrator.PSL.Common.LRS;

namespace PlayGen.Orchestrator.PSL.Contracts.LRS
{
	public class LRSPlayerSkill
	{
		public string Id;
		public LRSSkillVerb Skill;

		public override bool Equals(object other)
		{
			var otherSkill = other as LRSPlayerSkill;
			if (otherSkill == null)
				return false;
			return Id == otherSkill.Id && Skill == otherSkill.Skill;
		}
	}
}
