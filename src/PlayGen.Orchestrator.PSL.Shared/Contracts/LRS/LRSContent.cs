﻿namespace PlayGen.Orchestrator.PSL.Contracts.LRS
{
	public class LRSContent
	{
		public string Id;

		public LRSActor Actor;
		public LRSVerb Verb;
		public LRSResult Result;
		public LRSContext Context;

		public string TimeStamp;
		public string Version;
		public LRSObject Object;
	}
}