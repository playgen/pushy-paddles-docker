﻿using System.Collections.Generic;

namespace PlayGen.Orchestrator.PSL.Contracts.LRS
{
	public class LRSTeam
	{
	    public List<LRSActor> Member;
	    public string ObjectType;
	}
}
