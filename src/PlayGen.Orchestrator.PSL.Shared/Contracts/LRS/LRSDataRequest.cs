﻿using PlayGen.Orchestrator.PSL.Common.LRS;

namespace PlayGen.Orchestrator.PSL.Contracts.LRS
{
	public class LRSDataRequest
	{
		public string MatchId;

		public string PlayerId;

		public LRSSkillVerb Verb;

		public float Value;
	}
}
