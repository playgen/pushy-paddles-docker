﻿using System.Linq;
using System.Reflection;

namespace PlayGen.Orchestrator.PSL.Common.LRS
{
	public static class LRSSkillVerbExtensions
	{
		public static float GetMinRange(this LRSSkillVerb verb)
		{
			var fieldInfo = verb.GetType().GetField(verb.ToString());

			var attributes = (LRSSkillVerbAttribute[])fieldInfo.GetCustomAttributes(typeof(LRSSkillVerbAttribute), false);

			return attributes.Any() ? attributes.First().RangeMin : 0f;
		}

		public static bool GetUsesSocial(this LRSSkillVerb verb)
		{
			var fieldInfo = verb.GetType().GetField(verb.ToString());

			var attributes = (LRSSkillVerbAttribute[])fieldInfo.GetCustomAttributes(typeof(LRSSkillVerbAttribute), false);

			return attributes.Any() && attributes.First().UsesSocial;
		}
	}
}
