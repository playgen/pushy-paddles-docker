﻿namespace PlayGen.Orchestrator.PSL.Common.LRS
{
	public enum LRSSkillVerb
	{
		[LRSSkillVerb(0, false)] IntroducedSelf,
		[LRSSkillVerb(-1, true)] WasAssertive,
		[LRSSkillVerb(-1, true)] SaidNo,
		[LRSSkillVerb(-1, true)] AcceptedNo,
		[LRSSkillVerb(0, false)] IdentifiedSocialCues,
		[LRSSkillVerb(0, false)] ConcernedForFeelingsProtocol,
		[LRSSkillVerb(-1, false)] ConcernedForFeelingsResponse,
		[LRSSkillVerb(0, false)] DealtWithFeelings,
		[LRSSkillVerb(-1, true)] Helped,
		[LRSSkillVerb(0, true)] SolvedAsGroup,
		[LRSSkillVerb(-1, true)] Shared,
		[LRSSkillVerb(0, true)] Cooperated,
		[LRSSkillVerb(-1, true)] TookTurns,
		[LRSSkillVerb(-1, false)] SetGoal,
		[LRSSkillVerb(0, false)] MetGoal,
		[LRSSkillVerb(0, false)] StayedOnTask
	}
}
