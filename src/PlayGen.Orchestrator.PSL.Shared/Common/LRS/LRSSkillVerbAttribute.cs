﻿using System;

namespace PlayGen.Orchestrator.PSL.Common.LRS
{
	[AttributeUsage(AttributeTargets.Field)]
	public class LRSSkillVerbAttribute : Attribute
	{
		public float RangeMin { get; private set; }
		public bool UsesSocial { get; private set; }

		public LRSSkillVerbAttribute(float min, bool social)
		{
			RangeMin = min;
			UsesSocial = social;
		}
	}
}
