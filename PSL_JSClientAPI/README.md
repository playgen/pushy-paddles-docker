# PsL JavaScript game client API
## v0.1.0 (*alpha release*)

## Introduction 
This page describes the ProsocialLearn JavaScript game client API - it is intended for game developers to use.
Within this package is a demonstration page (index.html) that contains active code to demonstrate the basic application of the API.

### What does this API do for you?
This API allows you to:

* Access player and game instance meta-data from the PsL platform
* Stream audio (voice) data to the PsL platform for emotion analysis
* Stream visual (face) data to the PsL platform for emotion analysis
* Access local time offset of your player relative to the PsL platform time

## Licence 
The terms of use of this API are defined by the licence file entitled "PsL Game Client API Licence.txt" found in the licences folder of this package.

## Known issues
This is an alpha release of the API and has the following known issues:

* PsL back-end services that support this API are partially implemented and may be unavailable
* Time-stamps included in voice/face emotion data streams are not yet synchronized
* Validated & secured communications between the client and the platform are not yet implemented
* Browser support is currently limited to Chrome v55+

These issues will be resolved in a future release.

## Prerequisites 
The following important prerequisites must be met for the correct operation of this web page and the PsL game client API:

* Your computer can access the Internet and ProsocialLearn platform
* Your computer has either sound recording facilities (microphone) or video recording facilities (a webcam)
* This web page is being served to you by a web server (such as NGINX)
* You are viewing this page in Chrome

## Getting started
Follow these steps to get started:

1. Acquire the API from ProsocialLearn
2. Copy the 'pslLibs' folder into the javascript development folder you are working in.
3. Add the following script reference to your web page: ```<script src="<..your js folder..>/pslLibs/PSLClientAPI.js"></script>``` 
4. Load your web page and set up some test user data by executing the following on the web page console: ```__createTestUserCookie();``` 

You are now ready to start working with the PsL JavaScript game client API.

## Initialising the API
To initialise the API, you should instansiate a new instance of the API and then initialise it with the appropriate configuration for voice and/or face emotion capture (as required).

### Initialising for voice emotion capture
#### initialise( voiceConfig, null, initialiseCallback );

```
pslClient = new PSLClient();
        
var vConfig = { trackingCallback : onVoiceData };
        
pslClient.initialise( vConfig, null, onAPIInitialised );

...

function onAPIInitialised( result )
{
// result === 'OK'?
}

...

function onVoiceData( result )
{
// result === Float32Array
}
```

In the code above we see a new PSLClient instance is created and then initialised just for voice emotion detection. For voice configuration, you only need supply one parameter: 'trackingCallback' which points to a function allowing you to access captured audio data locally - this callback is optional and can be null. Above we supply 'null' for the second parameter to indicate that face based emotion detection is not needed. Finally, we point to our initialisation call-back function 'onAPIInitialised(..)' that gets called when the API has finished getting started - success is indicated with an 'OK' string.

### Initialising for face emotion capture
#### initialise( null, faceConfig, initialiseCallback );

```
pslClient = new PSLClient();
        
var fConfig = { videoID : 'myVideoInputDIV_ID',
                canvasID : 'myVideoOutputDIV_ID',
                width: 320,
                height: 240,
                trackingCallback : onFaceData };
        
pslClient.initialise( null, fConfig, onAPIInitialised );

...

function onAPIInitialised( result )
{
// result === 'OK'?
}

...

function onFaceData( result )
{
// result === array of ANEW-VA scores
}
```

In this example, we set up just for detecting emotion in the face via an attached web camera. Here configuration requires a few more elements, namely:

| config param(s)  | specification |
| ---------------- |-------------- |
| videoID          | ID of the ```<video>...</video>``` element used to render input captured from the web cam. A typical example of this would be: ```<video id="videoInput" width="320" height="240"><source></video>``` |
| canvasID         | ID of the ```<canvas>...</canvas>``` element used to render the augmented face feature analysis elements. A typical example of this would be: ```<canvas id="faceTrackOutput" width="320" height="240"></canvas>``` |
| width, height    | The width and height in pixels of the video stream input (in our example, this would be 320 and 240). |
| trackingCallback | A function that receives the emotion classification values as defined by the ANEW V-A space values. See the local callback function section below. This parameter can be null. |


In this case, when the initialisation function is called, we supply null for the voice configuration to indicate that voice based emotion capture is not required; again we use the 'onAPIInitialised(..)' function to get news of the result of the initialisation.
**Note: if you wish to use both voice and face emotion channels, simply supply both configuration objects in the same initialisation call.**

## Starting and stopping capture

#### trackVoice( true ); trackFace( true ); shutdown();

Once the API has been initialised, you can start and stop capture easily using the following methods:

```
pslClient.trackVoice( <boolean> );
```

Use this method to start tracking voice: true to start tracking, false to stop tracking.

```
pslClient.trackFace( <boolean> );
```

Use this method to start tracking the face: true to start tracking, false to stop tracking.

```
pslClient.shutdown();
```

Use this method to shutdown the client.

## Game information
#### getGameInfo() 

It is expected that your game will be deployed within the PsL platform and that players will gain access to it via the PsL portal. By the time a user has reached your game web page, the PsL platform will have information about who they are and which run-time instance of your game they will be linked with. The PsL Client API provides you with this information when you call the 'gameGameInfo()' method, returning:

```
{
playerNickName  : nick-name of player (string),
playerID        : unique identifier of player (UUID),
gameInstanceID  : unique identifier of game instance (UUID)
localTimeOffset : offset (in milliseconds) of your local clock relative to the PsL server
}
```

Note: if you are running this page locally or on a third party web server, this information is not directly provided to you via the platform. There are two work-arounds to generate some dummy data for this purpose:

* Call the __createTestUserCookie() function provided in our API

or

* Visit the PsL game store and then
    - Click 'Game Provider'
    - Click 'Test Game'
    - Enter the public URL of your game front-end
    - Enter a nick name
    - Click start

## Local callbacks
Local callbacks for locally accessing data generated for both voice and face are supported; you supply these callbacks during initialisation. The callback signatures are described below.

```
onVoiceCBFunc( result )
```

The result parameter is a Float32Array containing normalised floating-point PCM values (sampled at 44.1Khz).

```
onFaceCBFunc( result )
```

The result parameter is an array of ANEW-VA scores.