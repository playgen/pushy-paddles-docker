Licence to use IT Innovation/CERTH Software for carrying out the ProsocialLearn Project

DEFINITIONS

"Grant Agreement" means EC Grant Agreement 644204, relating to the Project
 ProsocialLearn - Gamification of Prosocial Learning for Increased Youth In-
clusion and Academic Achievement.

"Software" means the PSL Client API Javascript libraries and web content found
in this package and any documentation and any error corrections provided by 
IT Innovation/CERTH.

"IT Innovation" means the University of Southampton acting through the IT In-
novation Centre of Gamma House, Enterprise Road, Southampton SO16 7NS, UK.

"CERTH" means DEFINITION HERE

"You" means any Contractor identified in the Grant Agreement.

Words defined in the Grant Agreement or in the Consortium Agreement have the 
same meaning in this Licence.

ACCEPTANCE

By using the Software, You accept the terms of this Licence.

INTELLECTUAL PROPERTY RIGHTS    
The Software is IT Innovation Knowledge. The Software is confidential and 
copyrighted. Title to the Software and all associated intellectual property 
rights are retained by IT Innovation.

LICENCE 
IT Innovation grants You a free non-exclusive and non-transferable licence 
giving You Access Rights to the Software for carrying out the Project, as set 
out in the Grant Agreement Chapter 4 and the Consortium Agreement Section 9.

RESTRICTIONS
This Licence specifically excludes Access Rights for Use outside the Project 
as set out in the Consortium Agreement Section 9. You may not assign or 
transfer this Licence. You may not sublicense the Software. You may not make 
copies of the Software, other than for carrying out the Project and for 
backup purposes. You may not modify the Software.

LIABILITY
This Licence implies no warranty, as set out in the Consortium Agreement Sec-
tion 5.

TERMINATION
This Licence is effective until the end of the Project.  You may terminate 
this Licence at any time by written notice to IT Innovation. This Licence 
will terminate immediately without notice from IT Innovation if You fail to 
comply with any provision of this Licence. Upon Termination, You must destroy 
all copies of the Software.